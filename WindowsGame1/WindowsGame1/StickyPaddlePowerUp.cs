﻿using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using BreakOutUtil;

namespace BreakOutItem
{
    namespace BreakOutPowerUp
    {
        public class StickyPaddlePowerUp : PowerUp
        {
            private const string powerUpImageName = "PowerUp\\LowRes\\pu_stickyPaddle_LowRes";
            private const float internalZoom = 0.25f;

            public StickyPaddlePowerUp(GameModel model, Vector2 startPosition)
                : base(model, startPosition, powerUpImageName, internalZoom)
            {
                this.Timer = 15000; //15s
            }

            /// <summary>
            /// Power Up Starting effect
            /// </summary>
            public override void LaunchPower()
            {
                this.Model.Paddle.Sticky = true;
            }

            /// <summary>
            /// Power Up Stop effect used when the power up is timed.
            /// </summary>
            public override void StopPower()
            {
                foreach (Ball ball in this.Model.Balls)
                {
                    ball.CenterDifferenceWithPaddleCenter = null;
                }
                this.Model.Paddle.Sticky = false;
            }

            /// <summary>
            /// Resets the timer.
            /// </summary>
            public override void ResetTimer()
            {
                this.Timer = 1500;
            }
        }
    }
}
