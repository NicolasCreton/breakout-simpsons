﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutUtil
{
    public class PowerUpZoom
    {
        private float step;

        /// <summary>
        /// The step used to increment or decrement the zoom coefficient.
        /// </summary>
        /// <value>
        /// The step.
        /// </value>
        public float Step
        {
            get
            {
                return step;
            }
            set
            {
                step = value;
            }
        }

        /// <summary>
        /// Gets or sets the computed coefficient.
        /// </summary>
        /// <value>
        /// The computed coefficient.
        /// </value>
        private float computedCoefficient
        {
            get
            {
                return coefficient;
            }
            set
            {
                coefficient = value > this.MaxCoefficient ? this.MaxCoefficient : value;
                coefficient = coefficient < this.MinCoefficient ? this.MinCoefficient : coefficient;
            }
        }

        private float coefficient;

        /// <summary>
        /// Gets or sets the coefficient.
        /// </summary>
        /// <value>
        /// The coefficient.
        /// </value>
        public float Coefficient
        {
            get
            {
                return computedCoefficient;
            }
            set
            {
                if (value == 0)
                {
                    if (computedCoefficient < value)
                    {
                        computedCoefficient = -step;
                    }
                    else if (coefficient > value)
                    {
                        computedCoefficient = step;
                    }
                }
                else
                {
                    computedCoefficient = value;
                }
            }
        }

        /// <summary>
        /// Gets the zoom.
        /// </summary>
        /// <value>
        /// The zoom.
        /// </value>
        public float Zoom
        {
            get
            {
                float coef = Coefficient;
                if (coef < 0)
                {
                    return 1 / -coef;
                }
                return coef;
            }
        }

        /// <summary>
        /// Gets or sets the maximum increment.
        /// </summary>
        /// <value>
        /// The maximum increment.
        /// </value>
        public int MaxIncrement { get; set; }
        /// <summary>
        /// Gets the maximum coefficient.
        /// </summary>
        /// <value>
        /// The maximum coefficient.
        /// </value>
        public float MaxCoefficient
        {
            get
            {
                return 1 + MaxIncrement * step;
            }
        }

        /// <summary>
        /// Gets the minimum coefficient.
        /// </summary>
        /// <value>
        /// The minimum coefficient.
        /// </value>
        public float MinCoefficient
        {
            get
            {
                return 1 + MaxDecrement * step;
            }
        }

        /// <summary>
        /// Gets or sets the maximum decrement.
        /// </summary>
        /// <value>
        /// The maximum decrement.
        /// </value>
        public int MaxDecrement { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerUpZoom"/> class.
        /// </summary>
        /// <param name="maxDecrement">The maximum decrement.</param>
        /// <param name="maxIncrement">The maximum increment.</param>
        /// <param name="step">The step.</param>
        /// <param name="coefficient">The coefficient.</param>
        public PowerUpZoom(int maxDecrement, int maxIncrement, float step, float coefficient = 1)
        {
            this.coefficient = coefficient;
            this.Step = step;
            this.MaxDecrement = maxDecrement;
            this.MaxIncrement = maxIncrement;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerUpZoom"/> class.
        /// </summary>
        /// <param name="zoom">The zoom.</param>
        public PowerUpZoom(PowerUpZoom zoom)
            : this(zoom.MaxDecrement, zoom.MaxIncrement, zoom.Step, zoom.Coefficient)
        {

        }

        /// <summary>
        /// Implements the operator ++ to increment the zoom when called.
        /// </summary>
        /// <param name="zoom">The zoom.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static PowerUpZoom operator ++(PowerUpZoom zoom)
        {
            zoom.increment();
            return zoom;
        }

        /// <summary>
        /// Implements the operator -- to decrement the zoom when called.
        /// </summary>
        /// <param name="zoom">The zoom.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static PowerUpZoom operator --(PowerUpZoom zoom)
        {
            zoom.decrement();
            return zoom;
        }

        /// <summary>
        /// Increments the zoom.
        /// </summary>
        public void increment()
        {
            this.Coefficient = this.Coefficient + step;
        }

        /// <summary>
        /// Decrements the zoom.
        /// </summary>
        public void decrement()
        {
            this.Coefficient = this.Coefficient - step;
        }

        /// <summary>
        /// Sets to the minimum size.
        /// </summary>
        public void SetToMinimumSize()
        {
            this.Coefficient = this.MinCoefficient;
        }

        /// <summary>
        /// Sets to the maximum size.
        /// </summary>
        public void SetToMaximumSize() {
            this.Coefficient = this.MaxCoefficient;
        }

    }
}
