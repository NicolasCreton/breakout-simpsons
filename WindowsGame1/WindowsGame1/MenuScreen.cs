﻿using BreakOut;
using BreakOutModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using BreakOutScreen.ScreenButton;
using System.Collections.Generic;
using BreakOutResources;

namespace BreakOutScreen
{
    /// <summary>
    /// Subclass of the SpriteScreen
    /// This is the main menu screen
    /// </summary>
    public class MenuScreen : SpriteScreen
    {
        public ClickableButton StartGame { get; set; }
        public ClickableButton ExitGame { get; set; }
        public ClickableButton GameTitle { get; set; }
        public ClickableButton SettingsButton { get; set; }
        public ClickableButton instructionsButton { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuScreen"/> class.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <param name="model">The model.</param>
        public MenuScreen(Rectangle location, GameModel model)
            : base(model, location)
        {
            ButtonList.Add(this.GameTitle = new ClickableButton(GameModel.GameTitle, false, Color.Brown));
            ButtonList.Add(this.StartGame = new ClickableButton("Start Game", true, Color.Black));
            ButtonList.Add(this.instructionsButton = new ClickableButton("Instructions", true, Color.Black));
            ButtonList.Add(this.SettingsButton = new ClickableButton("Settings", true, Color.Black));
            ButtonList.Add(this.ExitGame = new ClickableButton("Exit the game", true, Color.Black));
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Loads the content.
        /// </summary>
        /// <param name="content">The content</param>
        /// Load the content of the screen like background and font of the different buttons
        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
            this.Texture = Resources.LoadImage("Pictures\\backMenu3", content);
            this.GameTitle.Font = this.TitleFont;
            this.StartGame.Font = this.NormalFont;
            this.instructionsButton.Font = this.NormalFont;
            this.SettingsButton.Font = this.NormalFont;
            this.ExitGame.Font = this.NormalFont;
            placeString(this.ButtonList, 100, 2, 4);
            PlayMusic();
        }

        /// <summary>
        /// Handles the input of the buttons in the screen
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyBordSate">The old key bord sate.</param>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        public override void HandleInput(KeyboardState keyboardState, KeyboardState oldKeyBordSate, MouseState mouseState, MouseState oldMouseState)
        {
            if (keyboardState.IsKeyDown(Keys.Enter))
            {
                MediaPlayer.Stop();
            }

            var mousePosition = new Point(mouseState.X, mouseState.Y);
            if (ExitGame.chekIfCliked(mousePosition, mouseState, oldMouseState, this.NormalFont) || keyboardState.IsKeyDown(Keys.Escape) && oldKeyBordSate.IsKeyUp(Keys.Escape))
            {
                MediaPlayer.Stop();
                this.Model.Game.Exit();
            }
            if (StartGame.chekIfCliked(mousePosition, mouseState, oldMouseState, NormalFont))
            {
                this.Model.Game.SelectedScreen = this.Model.Game.LevelScreen;
            }
            if (SettingsButton.chekIfCliked(mousePosition, mouseState, oldMouseState, NormalFont))
            {
                this.Model.Game.SelectedScreen = this.Model.Game.SettingsScreen;
            }
            if (instructionsButton.chekIfCliked(mousePosition, mouseState, oldMouseState, NormalFont))
            {
                this.Model.Game.SelectedScreen = this.Model.Game.InstructionScreen;
            }

            checkIfMouseOver(this.ButtonList, mousePosition, mouseState);

            base.HandleInput(keyboardState, oldKeyBordSate, mouseState, oldMouseState);
        }

        /// <summary>
        /// Updates the specified game time.
        /// This function updates the mouse position in the screen
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// Draws the cursor and the buttons in the screen
        /// The cursor chagnes when the mouse is over a ClickableButton
        /// </summary>
        /// <param name="spriteBatch">The sprite batch.</param>
        /// <param name="gameTime">The game time.</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {

            spriteBatch.Draw(this.Texture, this.Location, this.Tint);
            drawButtons(this.ButtonList, spriteBatch, 1f);

            base.Draw(spriteBatch, gameTime);
        }

        /// <summary>
        /// Plays the music.
        /// </summary>
        public void PlayMusic()
        {
            Song song = this.Model.Game.MenuTheme;
            if (song != null)
            {
                MediaPlayer.Play(this.Model.Game.MenuTheme);
            }
        }
    }
}
