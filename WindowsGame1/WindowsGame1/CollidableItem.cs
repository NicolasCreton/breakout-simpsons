﻿using BreakOutUtil;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOut;
using BreakOutModel;

namespace BreakOutItem
{
    public abstract class CollidableItem : Item
    {
        /// <summary>
        /// Gets the hit box. Used to know an item intersects another item.
        /// </summary>
        /// <value>
        /// The hit box.
        /// </value>
        public Rectangle HitBox
        {
            get
            {
                Vector2 position = this.Position;
                Dimension size = new Dimension();
                size.Width = Convert.ToInt32(this.Size.Width);
                size.Height = Convert.ToInt32(this.Size.Height);
                return new Rectangle((int)position.X, (int)position.Y, (int)size.Width, (int)size.Height);
            }
        }

        public CollidableItem(GameModel model, Vector2 position, string imageName, float internalZoom, PowerUpZoom powerUpZoom)
            : base(model, position, imageName, internalZoom, powerUpZoom)
        {

        }

        /// <summary>
        /// Checks if the hit box of 2 items intersect.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns></returns>
        public bool Intersects(CollidableItem item)
        {
            return this.HitBox.Intersects(item.HitBox);
        }
    }
}
