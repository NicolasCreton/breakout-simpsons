﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutScreen;
using Microsoft.Xna.Framework;
using BreakOut;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using BreakOutModel;
using BreakOutLevel;
using BreakOutScreen.ScreenButton;
using BreakOutResources;

namespace BreakOutScreen
{
    /// <summary>
    /// Subclass of the SpriteScreen
    /// This a level chooser screen
    /// This screen will read the different levels in folders and create buttons.
    /// </summary>
    public class LevelScreen : SpriteScreen
    {
        public ClickableButton ChooseLevel { get; set; }
        public List<ClickableButtonLevel> LevelList { get; set; }
        public ClickableButton ReturnButton { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LevelScreen"/> class.
        /// </summary>
        /// Read the level folder and create buttons thanks to this folder
        /// <param name="location">The location.</param>
        /// <param name="model">The model.</param>
        public LevelScreen(Rectangle location, GameModel model)
            : base(model, location)
        {
            this.ChooseLevel = new ClickableButton("Choose a level", false, Color.Brown);
            this.ButtonList.Add(this.ChooseLevel);
            this.ReturnButton = new ClickableButton("Return (Escape)", true, Color.Tomato);
            this.ButtonList.Add(this.ReturnButton);
            this.LevelList = new List<ClickableButtonLevel>();
            string[] levelFiles = this.Model.LevelManager.getListOfLevelFiles();
            int levelInDirectory = levelFiles.Length;

            for (int level = 0; level < levelInDirectory; level++)
            {
                string levelName = LevelManager.GetName(levelFiles[level]);
                ClickableButtonLevel levelX = new ClickableButtonLevel(levelName, true, Color.Black, level);
                this.LevelList.Add(levelX);
            }
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Loads the content.
        /// </summary>
        /// <param name="content">The content</param>
        /// Load the content of the screen like background and font of the different buttons
        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
            this.Texture = Resources.LoadImage("Pictures\\backLevel", this.Model.Game);
            this.ChooseLevel.Font = this.TitleFont;
            this.ReturnButton.Font = this.NormalFont;
            foreach (ClickableButtonLevel button in this.LevelList)
            {
                button.Font = this.NormalFont;
            }
            placeString(this.ButtonList, 70, 1.4f, 5);
            placeString(this.LevelList, 70, 1.4f, 5);
        }

        /// <summary>
        /// Handles the input in the screen
        /// Load the clicked level thanks to the LevelNuber in the button
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyBordSate">The old key bord sate.</param>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        public override void HandleInput(KeyboardState keyboardState, KeyboardState oldKeyBordSate, MouseState mouseState, MouseState oldMouseState)
        {
            var mousePosition = new Point(mouseState.X, mouseState.Y);
            checkIfMouseOver(this.LevelList, mousePosition, mouseState);
            if (this.MouseOver == false)
            {
                checkIfMouseOver(this.ButtonList, mousePosition, mouseState);
            }


            foreach (ClickableButtonLevel levelButton in this.LevelList)
            {
                if (levelButton.chekIfCliked(mousePosition, mouseState, oldMouseState, levelButton.Font))
                {
                    MediaPlayer.Stop();
                    Song song = this.Model.Game.GameTheme;
                    if (song != null)
                    {
                        MediaPlayer.Play(song);
                    }
                    this.Model.actualLevelIndex = levelButton.LevelNumber;
                    this.Model.LoadModel(levelButton.LevelNumber);
                    this.Model.ResetLevel();
                    this.Model.Game.StartGame();
                    this.Model.Game.SelectedScreen = this.Model.Game.GameScreen;
                }

                if (keyboardState.IsKeyDown(Keys.Escape) && oldKeyBordSate.IsKeyUp(Keys.Escape) || ReturnButton.chekIfCliked(mousePosition, mouseState, oldMouseState, ReturnButton.Font))
                {
                    this.Model.Game.SelectedScreen = this.Model.Game.MenuScreen;
                }
            }

            base.HandleInput(keyboardState, oldKeyBordSate, mouseState, oldMouseState);
        }

        /// <summary>
        /// Draws the cursor and the buttons in the screen
        /// The cursor chagnes when the mouse is over a ClickableButton
        /// </summary>
        /// <param name="spriteBatch">The sprite batch.</param>
        /// <param name="gameTime">The game time.</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(this.Texture, this.Location, this.Tint);
            drawButtons(this.ButtonList, spriteBatch, 1f);
            drawButtons(this.LevelList, spriteBatch, 1f);
            base.Draw(spriteBatch, gameTime);
        }

        /// <summary>
        /// Updates the specified game time.
        /// This function updates the mouse position in the screen
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }

}
