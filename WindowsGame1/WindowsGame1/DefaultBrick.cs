﻿using BreakOut;
using BreakOutUtil;
using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutItem
{
    namespace BreakOutBrick
    {
        class DefaultBrick : Brick
        {
            /// <summary>
            /// Gets the symbol used to generate levels.
            /// </summary>
            /// <value>
            /// The symbol.
            /// </value>
            public static char Symbol
            {
                get { return '#'; }
            }

            private const float internalZoom = 0.28f;

            public const String BrickImageName = "bricks\\LowRes\\b_standard_LowRes";

            public DefaultBrick(GameModel model, Vector2 position, TabCoordinates coordinates)
                : base(model, position, coordinates, BrickImageName, null, internalZoom)
            {
                this.Solid = true;
                this.Lives = 1;
            }
        }
    }
}
