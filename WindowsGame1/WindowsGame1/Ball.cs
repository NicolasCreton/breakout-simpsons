﻿using BreakOutUtil;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using BreakOut;
using BreakOutModel;
using BreakOutItem.BreakOutPowerUpTags;
using BreakOutItem.BreakOutBrick;
using BreakOutItem.BreakOutItemTags;
using System.Diagnostics;

namespace BreakOutItem
{
    public class Ball : FallingItem, ResizableItem, SpeedVariatingItem
    {
        private const string ballImageName = "ball2_LowRes";
        /// <summary>
        /// The ball intial speed when beginning a level
        /// </summary>
        private const float ballStartSpeed = 0.75f;
        /// <summary>
        /// The internal zoom - Zoom used to scale the picture from its original size to its game size
        /// </summary>
        private const float internalZoom = 0.235f;

        public CollisionType LastCollision { get; set; }
        /// <summary>
        /// Gets or sets the position difference with paddle.
        /// </summary>
        /// <value>
        /// The position difference with paddle. When the Stick Paddle Power Up is enabled, the ball must stick to the paddle. This variable is then defined. Otherwise, the variable is null.
        /// </value>
        public Vector2? CenterDifferenceWithPaddleCenter { get; set; }

        public bool Sticked
        {
            get
            {
                return this.CenterDifferenceWithPaddleCenter != null;
            }
        }



        /// <summary>
        /// Initializes a new instance of the <see cref="Ball"/> class, with a vertial moving direction, and sticked to the paddle.
        /// </summary>
        /// <param name="model">The model.</param>
        public Ball(GameModel model)
            : this(model, new Vector2(), new VaryingSpeed(model, ballStartSpeed, new Vector2(0, -1), 3, 3, 0.1f), new PowerUpZoom(-3, 3, 0.2f))
        {
            this.SetInStartPosition(model.Paddle);
        }


        /// <summary>
        /// Initializes a new instance of the <see cref="Ball"/> class, with a certain position, a certain speed, and a certain size. This constructor is mainly used when cloning balls (DoubleBallPowerUp, TripleBallPowerUp, NineMiniBallsPowerUp).
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="position">The position.</param>
        /// <param name="speed">The speed.</param>
        /// <param name="powerUpZoom">The power up zoom.</param>
        public Ball(GameModel model, Vector2 position, VaryingSpeed speed, PowerUpZoom powerUpZoom)
            : base(model, position, ballImageName, speed, internalZoom, powerUpZoom)
        {
            this.Model = model;
        }



        /// <summary>
        /// Updates according to the specified game elapsed time since last update. Moves the ball, checks if there is a collision, or if the ball is off limits.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            if (this.Model.Game.GameScreen.Clicked)
            {
                this.CenterDifferenceWithPaddleCenter = null;
            }
            if (!this.Sticked)
            {
                this.Step(gameTime);

                this.CheckOffBottom();
                if (!this.Over)
                {
                    this.CheckWallCollision();
                    this.CheckBricksCollision();
                    this.CheckPaddleCollision();
                }
            }
            else
            {
                this.UpdateStickedPosition();
            }
        }

        /// <summary>
        /// Updates the sticked position relative to the paddle. Used when the ball is sticked to the paddle.
        /// </summary>
        private void UpdateStickedPosition()
        {
            Vector2 paddleCenter = this.Model.Paddle.Center;
            this.Center = paddleCenter + this.CenterDifferenceWithPaddleCenter.Value;
        }

        /// <summary>
        /// Checks if the ball collides with any brick.
        /// </summary>
        private void CheckBricksCollision()
        {
            foreach (Brick brick in Model.Level.Bricks)
            {
                if (brick != null)
                {
                    brick.CheckCollision(this);
                }
            }
        }

        /// <summary>
        /// Checks if the ball fell down, and removes it if so.
        /// </summary>
        private void CheckOffBottom()
        {
            if (this.OffBottom())
            {
                this.Destroy();
            }
        }

        /// <summary>
        /// Puts back the ball within the screen borders
        /// Checks the wall collision and sets the moving direction depending of the incidence direction
        /// </summary>
        private void CheckWallCollision()
        {
            if (this.Position.X < 0 && this.Speed.MovingDirection.X < 0)
            {
                this.moveTo(0, this.Position.Y);
                this.Speed.MultiplyMovingDirection(-1, 1);
                this.LastCollision = CollisionType.Wall;
            }
            if (this.Position.X + this.Size.Width > this.Model.Game.ScreenBounds.Width && this.Speed.MovingDirection.X > 0)
            {
                this.moveTo(this.Model.Game.ScreenBounds.Width - this.Size.Width, this.Position.Y);
                this.Speed.MultiplyMovingDirection(-1, 1);
                this.LastCollision = CollisionType.Wall;
            }
            if (this.Position.Y < 0 && this.Speed.MovingDirection.Y < 0)
            {
                this.moveTo(Position.X, 0);
                this.Speed.MultiplyMovingDirection(1, -1);
                this.LastCollision = CollisionType.Wall;
            }
        }

        /// <summary>
        /// Sets the ball in its start position, on the middle of the paddle.
        /// </summary>
        /// <param name="paddle">The paddle.</param>
        public void SetInStartPosition(Paddle paddle)
        {
            this.SetStartPosition(paddle);
        }

        /// <summary>
        /// Not used. Sets the starting moving direction to a random value. Unused since we make it go vertical.
        /// </summary>
        private void SetRandomStartMovingDirection()
        {
            Random rand = new Random();
            Vector2 motion = new Vector2(rand.Next(2, 6), -rand.Next(2, 6));
            motion.Normalize();
            this.Speed.MovingDirection = motion;
        }


        /// <summary>
        /// Sets the start position to the center of the paddle.
        /// </summary>
        /// <param name="paddle">The paddle.</param>
        private void SetStartPosition(Paddle paddle)
        {
            Vector2 paddlePosition = paddle.Position;
            Dimension paddleSize = paddle.Size;
            this.CenterDifferenceWithPaddleCenter = new Vector2(0, -(paddle.Size.Height + this.Size.Height) / 2);
        }

        /// <summary>
        /// Sticks the ball to the paddle;
        /// </summary>
        private void SetStickedPosition()
        {
            Paddle paddle = this.Model.Paddle;
            Vector2 difference = new Vector2(paddle.Center.X - this.Center.X, paddle.Position.Y - this.Size.Height / 2);
            this.CenterDifferenceWithPaddleCenter = difference;
        }

        /// <summary>
        /// Changes the ball's direction depending on the collision coordinate X on the racket (paddle), to get a variable reflection angle
        /// </summary>
        public override void HitPaddle()
        {
            if (this.Model.Paddle.Sticky)
            {
                this.Model.Paddle.IsLaunched = false;
            }
            this.Model.Paddle.Hit(this);
            this.LastCollision = CollisionType.Paddle;
            
        }

        /// <summary>
        /// Puts back the ball out of the brick's hitbox
        /// Reflects the ball depending on the impact position on the brick.
        /// </summary>
        /// <param name="brick">The brick.</param>
        public void HitBrick(Brick brick)
        {
            brick.Hit(this);
        }

        public override bool OffBottom()
        {
            bool offBottom = base.OffBottom();
            if (offBottom && this.Model.Paddle.BallSavior)
            {
                this.moveTo(this.Position.X, this.Model.Game.ScreenBounds.Height);
                this.Speed.MultiplyMovingDirection(1, -1);
                this.Model.Paddle.BallSavior = false;
                return false;
            }
            return offBottom;
        }

        /// <summary>
        /// Grows this ball size.
        /// </summary>
        public void Grow()
        {
            this.Zoom.PowerUpZoom++;
        }

        /// <summary>
        /// Reduces the ball size.
        /// </summary>
        public void Shrink()
        {
            this.Zoom.PowerUpZoom--;
        }

        /// <summary>
        /// Accelerates the ball.
        /// </summary>
        public void Accelerate()
        {
            if (this.Speed is VaryingSpeed)
            {
                VaryingSpeed speed = (VaryingSpeed)this.Speed;
                speed++;
            }
        }

        /// <summary>
        /// Slows down the ball.
        /// </summary>
        public void SlowDown()
        {
            if (this.Speed is VaryingSpeed)
            {
                VaryingSpeed speed = (VaryingSpeed)this.Speed;
                speed--;
            }
        }

        /// <summary>
        /// Resets the ball to its initial state (on the paddle, with its initial size and speed.
        /// </summary>
        /// <param name="paddle">The paddle.</param>
        public void Reset(Paddle paddle)
        {
            this.Zoom.PowerUpZoom = new PowerUpZoom(-3, 3, 0.2f);
            this.SetInStartPosition(paddle);
            this.Speed.Value = ballStartSpeed;
        }

        /// <summary>
        /// Pop two balls out of this one.
        /// </summary>
        /// <returns></returns>
        public LinkedList<Ball> DoubleBall()
        {
            this.Model.Balls.Remove(this);
            return this.MultiplyNbBallsBy(2);
        }

        /// <summary>
        /// Pop 3 balls out of this one.
        /// </summary>
        /// <returns></returns>
        public LinkedList<Ball> TripleBall()
        {
            this.Model.Balls.Remove(this);
            return this.MultiplyNbBallsBy(3);
        }

        /// <summary>
        /// Pop nbBalls out of this one.
        /// </summary>
        /// <param name="nbBalls">The nb balls.</param>
        /// <returns></returns>
        public LinkedList<Ball> MultiplyNbBallsBy(int nbBalls)
        {
            LinkedList<Ball> balls = new LinkedList<Ball>();
            double theta = this.Speed.Angle;
            if (theta < 0)
            {
                theta += 2 * Math.PI;
            }
            double rotationDivider = nbBalls % 2 == 0 ? nbBalls + 2 : nbBalls + 1;
            double negativeRotationAngle = theta;
            double positiveRotationAngle = theta;

            if (nbBalls % 2 == 1) //If nbBalls is odd, we add the original ball
            {
                balls.AddLast(this);
                this.Model.Balls.AddLast(this);
            }

            double limit = theta + Math.PI / 2;
            positiveRotationAngle += Math.PI / rotationDivider;
            negativeRotationAngle -= Math.PI / rotationDivider;

            while (positiveRotationAngle < limit)
            {
                Ball positiveBall = new Ball(this.Model, new Vector2(Position.X, Position.Y), new VaryingSpeed((VaryingSpeed)this.Speed), new PowerUpZoom(this.Zoom.PowerUpZoom));
                positiveBall.Speed.Angle = positiveRotationAngle;
                this.Model.Balls.AddLast(positiveBall);
                balls.AddLast(positiveBall);

                Ball negativeBall = new Ball(this.Model, new Vector2(Position.X, Position.Y), new VaryingSpeed((VaryingSpeed)this.Speed), new PowerUpZoom(this.Zoom.PowerUpZoom));
                negativeBall.Speed.Angle = negativeRotationAngle;
                this.Model.Balls.AddLast(negativeBall);
                balls.AddLast(negativeBall);

                positiveRotationAngle += Math.PI / rotationDivider;
                negativeRotationAngle -= Math.PI / rotationDivider;
            }

            return balls;
        }



        /// <summary>
        /// Removes the ball from the model
        /// </summary>
        public void Destroy()
        {
            this.Over = true;
            this.Model.RemoveBall(this);
        }
    }
}
