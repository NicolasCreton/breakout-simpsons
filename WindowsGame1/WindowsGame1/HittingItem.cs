﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutItem
{
    namespace BreakOutItemTags
    {
        /// <summary>
        /// Shows that the class can hit a ball.
        /// </summary>
        public interface HittingItem
        {
            void Hit(Ball ball);
            void HitTop(Ball ball);
            void HitLeft(Ball ball);
            void HitRight(Ball ball);
            void HitBottom(Ball ball);
        }
    }
}