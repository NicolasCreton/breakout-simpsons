﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutUtil
{
    /// <summary>
    /// Informs whether the collision is with a paddle, a wall, a brick or a ball.
    /// </summary>
    public enum CollisionType
    {
        Paddle = 1, Wall = 2, Brick = 3, Ball = 4
    }
}
