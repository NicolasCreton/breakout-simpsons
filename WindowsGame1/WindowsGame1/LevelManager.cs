﻿using BreakOutItem;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using BreakOut;
using BreakOutModel;
using BreakOutUtil;
using System.Diagnostics;
using BreakOutItem.BreakOutBrick;

namespace BreakOutLevel
{
    public class LevelManager
    {
        Dictionary<char, Type> brickDictionary = new Dictionary<char, Type>();

        private const string nativelevelPath = "Content/Level/";
        private string customLevelPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "/HomersBreak/Level/";
        private const string extension = "txt";
        private GameModel model;
        private List<Type> brickClasses { get; set; }

        public LevelManager(GameModel model)
        {
            this.model = model;
            this.ListBrickClasses();
        }

        /// <summary>
        /// Gets the list of level files.
        /// </summary>
        /// <returns></returns>
        public string[] getListOfLevelFiles()
        {
            string[] nativeLevels = Directory.GetFiles(nativelevelPath, "*." + extension, SearchOption.AllDirectories);

            if (Directory.Exists(customLevelPath))
            {
                return nativeLevels.Concat(Directory.GetFiles(customLevelPath, "*." + extension, SearchOption.AllDirectories)).ToArray();
            }
            return nativeLevels;
        }

        public static string GetName(string fileName)
        {
            return Path.GetFileNameWithoutExtension(fileName);
        }

        /// <summary>
        /// Gets the level from the given file name.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        public Level getLevel(string fileName, GameModel model)
        {
            string[] lines = System.IO.File.ReadAllLines(fileName);

            int maxLength = getMaxLength(lines);
            char[,] bricksSymbols = new char[lines.Length, maxLength];
            for (int i = 0; i < lines.Length; i++)
            {
                char[] brickLine = lines[i].ToCharArray();
                for (int j = 0; j < brickLine.Length; j++)
                {
                    bricksSymbols[i, j] = brickLine[j];
                }
            }
            Level level = new Level(model, LevelManager.GetName(fileName), bricksSymbols);
            return level;
        }

        /// <summary>
        /// Gets the maximum length.
        /// </summary>
        /// <param name="tab">The tab.</param>
        /// <returns></returns>
        private int getMaxLength(string[] tab)
        {
            int size = 0;
            foreach (string line in tab)
            {
                size = line.Length > size ? line.Length : size;
            }
            return size;
        }

        /// <summary>
        /// Convert char to brick.
        /// </summary>
        /// <param name="bricksSymbols">The bricks symbols.</param>
        /// <returns></returns>
        public Brick[,] toBricks(char[,] bricksSymbols, GameModel model)
        {
            int brickNumberWidth = bricksSymbols.GetLength(1);
            int brickNumberHeight = bricksSymbols.GetLength(0);

            int screenWidth = 1920;
            int screenHeight = 1080;

            int marginSide = Convert.ToInt32(0.025f * screenWidth);
            int marginTop = Convert.ToInt32(0.025f * screenHeight);

            int brickWidth = Convert.ToInt32((screenWidth - 2 * marginSide) / brickNumberWidth);
            int brickHeight = Convert.ToInt32((screenHeight / 2 - marginTop) / brickNumberHeight);


            Brick[,] bricks = new Brick[bricksSymbols.GetLength(0), bricksSymbols.GetLength(1)];
            for (int i = 0; i < bricksSymbols.GetLength(0); i++)
            {
                for (int j = 0; j < bricksSymbols.GetLength(1); j++)
                {
                    object[] args = new object[] { model, new Vector2(j * brickWidth + marginSide, i * brickHeight + marginTop), new TabCoordinates(i, j) };
                    bricks[i, j] = this.makeBrick(bricksSymbols[i, j], args);
                }
            }
            return bricks;
        }

        /// <summary>
        /// Lists the brick classes and adds to the [symbol char->brick classes] dictionary (hash table).
        /// </summary>
        private void ListBrickClasses()
        {
            List<Type> brickClasses = (AppDomain.CurrentDomain.GetAssemblies()
                       .SelectMany(t => t.GetTypes())
                       .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(Brick)) /*&& t.Namespace == "BreakOutItem.BreakOutBrick"*/)).ToList<Type>();

            foreach (Type type in brickClasses)
            {
                char? pSymbol = (char)type.GetProperty("Symbol").GetValue(null, null);
                if (pSymbol != null)
                {
                    this.brickDictionary.Add(pSymbol.Value, type);
                }
                else
                {
                    Debug.WriteLine("Your Bricks need a static Symbol property.");
                }
            }
        }

        /// <summary>
        /// Makes an instance of the brick specified by the given symbol 
        /// </summary>
        /// <param name="symbol">The symbol.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public Brick makeBrick(char symbol, object[] args)
        {
            try
            {
                Type type = this.brickDictionary[symbol];
                return makeBrick(type, args);
            }
            catch (KeyNotFoundException ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Makes an instance of the specified type of brick with the args arguments in constructor
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        public Brick makeBrick(Type type, object[] args)
        {
            return (Brick)Activator.CreateInstance(type, args);
        }
    }
}
