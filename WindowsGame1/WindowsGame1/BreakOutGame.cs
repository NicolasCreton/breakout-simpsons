using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using BreakOutItem;
using BreakOutScreen;
using BreakOutLevel;
using BreakOutModel;
using System.Diagnostics;
using BreakOutResources;

namespace BreakOut
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class BreakOutGame : Game
    {

        /// <summary>
        /// The sprite batch. It is declared so as not to make a new spriteBatch variable everytime.
        /// </summary>
        private SpriteBatch spriteBatch;
        /// <summary>
        /// The current keyboard state
        /// </summary>
        private KeyboardState keyboardState;
        /// <summary>
        /// The previous keyboard state so as to compare if the input changed (and not repeat a same action twice)
        /// </summary>
        private KeyboardState oldKeyboardState;
        /// <summary>
        /// The current mouse state
        /// </summary>
        private MouseState mouseState;
        /// <summary>
        /// The previous mouse state so as to compare if the input changed (and not repeat a same action twice)
        /// </summary>
        private MouseState oldMouseState;

        /// <summary>
        /// The actual screen
        /// </summary>
        private SpriteScreen selectedScreen;

        /// <summary>
        /// Gets or sets the screen bounds
        /// </summary>
        /// <value>
        /// The screen bounds.
        /// </value>
        public Rectangle ScreenBounds { get; set; }
        /// <summary>
        /// Gets or sets the model. The model is all the data needed for the application to work
        /// </summary>
        /// <value>
        /// The model.
        /// </value>
        public GameModel Model { get; set; }
        public GraphicsDeviceManager Graphics { get; set; }
        /// <summary>
        /// Gets or sets the menu theme.
        /// </summary>
        /// <value>
        /// The menu theme.
        /// </value>
        public Song MenuTheme { get; set; }
        /// <summary>
        /// Gets or sets the game theme.
        /// </summary>
        /// <value>
        /// The game theme.
        /// </value>
        public Song GameTheme { get; set; }
        /// <summary>
        /// Gets or sets the pause screen.
        /// </summary>
        /// <value>
        /// Pause Screen - Used to pause from the game screen.
        /// </value>
        public PauseScreen PauseScreen { get; set; }
        /// <summary>
        /// Menu Screen, first screen displayed at game launch.
        /// </summary>
        /// <value>
        /// Menu Screen, first screen displayed at game launch.
        /// </value>
        public MenuScreen MenuScreen { get; set; }
        /// <summary>
        /// Gets or sets the level screen.
        /// </summary>
        /// <value>
        /// Level Screen, used to choose the level you want to play.
        /// </value>
        public LevelScreen LevelScreen { get; set; }
        /// <summary>
        /// Settings Screen
        /// </summary>
        /// <value>
        /// The settings screen.
        /// </value>
        public SettingsScreen SettingsScreen { get; set; }
        /// <summary>
        /// Won Screen
        /// </summary>
        /// <value>
        /// The won screen.
        /// </value>
        public WonScreen WonScreen { get; set; }
        /// <summary>
        /// Gets or sets the loose screen.
        /// </summary>
        /// <value>
        /// The loose screen.
        /// </value>
        public LooseScreen LooseScreen { get; set; }
        /// <summary>
        /// Gets or sets the game screen.
        /// </summary>
        /// <value>
        /// The game screen.
        /// </value>
        public GameScreen GameScreen { get; set; }
        /// <summary>
        /// Gets or sets the about screen.
        /// </summary>
        /// <value>
        /// The about screen.
        /// </value>
        public AboutScreen AboutScreen { get; set; }
        /// <summary>
        /// Gets or sets the instruction screen.
        /// </summary>
        /// <value>
        /// The instruction screen : Bricks instruction screen
        /// </value>
        public FirstScreenInstructions InstructionScreen { get; set; }
        /// <summary>
        /// Gets or sets the second instruction screen.
        /// </summary>
        /// <value>
        /// The second instruction screen : PowerUp Instruction screen
        /// </value>
        public SecondInstructionScreen SecondInstructionScreen { get; set; }
        /// <summary>
        /// Gets or sets the selected screen.
        /// </summary>
        /// <value>
        /// The selected screen.
        /// </value>
        public SpriteScreen SelectedScreen
        {
            get
            {
                return selectedScreen;
            }
            set
            {
                selectedScreen = value; selectedScreen.UpdateWindowTitle();
            }
        }
        public List<SpriteScreen> ScreenList { get; set; }

        public BreakOutGame()
        {
            this.Graphics = new GraphicsDeviceManager(this);
            this.Graphics.ToggleFullScreen();
            Content.RootDirectory = "Content";
            this.Graphics.PreferredBackBufferWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
            this.Graphics.PreferredBackBufferHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
            this.ScreenBounds = new Rectangle(0, 0, this.Graphics.PreferredBackBufferWidth, this.Graphics.PreferredBackBufferHeight);
            this.ScreenList = new List<SpriteScreen>();
            this.Model = new GameModel(this);
            this.Window.Title = GameModel.GameTitle;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.ScreenList.Add(this.MenuScreen = new MenuScreen(this.ScreenBounds, this.Model));
            this.ScreenList.Add(this.SettingsScreen = new SettingsScreen(this.ScreenBounds, this.Model));
            this.ScreenList.Add(this.WonScreen = new WonScreen(this.ScreenBounds, this.Model));
            this.ScreenList.Add(this.LooseScreen = new LooseScreen(this.ScreenBounds, this.Model));
            this.ScreenList.Add(this.PauseScreen = new PauseScreen(this.ScreenBounds, this.Model));
            this.ScreenList.Add(this.AboutScreen = new AboutScreen(this.ScreenBounds, this.Model));
            this.ScreenList.Add(this.LevelScreen = new LevelScreen(this.ScreenBounds, this.Model));
            this.ScreenList.Add(this.InstructionScreen = new FirstScreenInstructions(this.ScreenBounds, this.Model));
            this.ScreenList.Add(this.SecondInstructionScreen = new SecondInstructionScreen(this.ScreenBounds, this.Model));
            this.ScreenList.Add(this.GameScreen = new GameScreen(this.Model));

            this.ScreenList.ForEach(screen => screen.Initialize());
            this.SelectedScreen = this.MenuScreen;
            base.Initialize();

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            this.spriteBatch = new SpriteBatch(GraphicsDevice);
            this.MenuTheme = Resources.LoadSong("Sounds\\Theme", this);
            this.GameTheme = Resources.LoadSong("Sounds\\GameTheme", this);
            foreach (SpriteScreen screen in this.ScreenList)
            {
                screen.LoadContent(Content);
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        public void StartGame()
        {
            this.Model.Paddle.SetInStartPosition();
            foreach (Ball ball in this.Model.Balls)
            {
                ball.SetInStartPosition(this.Model.Paddle);
            }
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            this.keyboardState = Keyboard.GetState();
            this.mouseState = Mouse.GetState();
            var mousePosition = new Point(mouseState.X, mouseState.Y);
            this.SelectedScreen.HandleInput(this.keyboardState, this.oldKeyboardState, this.mouseState, this.oldMouseState);
            if (this.SelectedScreen == this.GameScreen || this.SelectedScreen == this.PauseScreen)
            {
                this.PauseScreen.togglePause(this.keyboardState, this.oldKeyboardState);
            }
            this.SettingsScreen.switchScreenSizeFromAnywhere(keyboardState, oldKeyboardState);
            this.SettingsScreen.switchMuteFromAnywhere(keyboardState, oldKeyboardState);
            this.SelectedScreen.Update(gameTime);
            base.Update(gameTime);
            this.oldMouseState = this.mouseState;
            this.oldKeyboardState = this.keyboardState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            this.IsMouseVisible = false;
            this.spriteBatch.Begin();
            if (this.SelectedScreen == this.GameScreen)
            {
                this.GameScreen.DrawGame(this.spriteBatch, gameTime);
            }
            else
            {
                this.SelectedScreen.Draw(this.spriteBatch, gameTime);
            }
            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}