﻿using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutUtil;

namespace BreakOutItem
{
    namespace BreakOutPowerUp
    {
        public class GrowPaddlePowerUp : PowerUp
        {
            private const string powerUpImageName = "PowerUp\\LowRes\\pu_bigPaddle_LowRes";
            private const float internalZoom = 0.25f;

            public GrowPaddlePowerUp(GameModel model, Vector2 startPosition)
                : base(model, startPosition, powerUpImageName, internalZoom)
            {

            }

            /// <summary>
            /// Power Up Starting effect
            /// </summary>
            public override void LaunchPower()
            {
                this.Model.Paddle.Grow();
            }
        }
    }
}
