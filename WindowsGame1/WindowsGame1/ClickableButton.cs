﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BreakOutScreen
{
    namespace ScreenButton
    {
        /// <summary>
        /// Base class of the buttons
        /// This class represents a clickable button or a non clickable string
        /// </summary>
        public class ClickableButton
        {
            public Vector2 Origin { get; set; }
            public string TextButton { get; set; }
            public SpriteFont Font { get; set; }
            public bool IsClickable { get; set; }
            public Color TextColor { get; set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="ClickableButton"/> class.
            /// </summary>
            /// <param name="text">The text.</param>
            /// <param name="clickable">if set to <c>true</c> [clickable].</param>
            /// <param name="color">The color.</param>
            public ClickableButton(string text, bool clickable, Color color)
            {
                this.TextButton = text;
                this.IsClickable = clickable;
                this.TextColor = color;
            }

            /// <summary>
            /// Places the font of the string thanks to the spacing and the x and y divisor
            /// The strings are placed thanks to the screen resolutions to be always visible
            /// </summary>
            /// <param name="ySpacing">The y spacing.</param>
            /// <param name="xDivisor">The x divisor.</param>
            /// <param name="yDivisor">The y divisor.</param>
            public virtual void placeFont(int ySpacing, float xDivisor, float yDivisor)
            {
                var textSize = this.Font.MeasureString(this.TextButton);
                var screenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                var screenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                float textOriginX = (screenWidth / xDivisor) - (textSize.X / 2);
                float textOriginY = (screenHeight / yDivisor) + ySpacing;
                this.Origin = new Vector2(textOriginX, textOriginY);
            }

            /// <summary>
            /// Draws the buttons with their colors and scale
            /// </summary>
            /// <param name="batch">The batch.</param>
            /// <param name="scale">The scale.</param>
            /// <param name="textColor">Color of the text.</param>
            public virtual void Draw(SpriteBatch batch, float scale, Color textColor)
            {
                batch.DrawString(this.Font, this.TextButton, this.Origin, textColor, 0, new Vector2(0, 0), scale, SpriteEffects.None, 0f);
            }

            /// <summary>
            /// Checks if mouse over.
            /// </summary>
            /// <param name="mousePosition">The mouse position.</param>
            /// <param name="mouseState">State of the mouse.</param>
            /// <param name="font">The font.</param>
            /// <returns></returns>
            public virtual bool checkIfMouseOver(Point mousePosition, MouseState mouseState, SpriteFont font)
            {
                var stringSize = font.MeasureString(this.TextButton);
                Rectangle overlayRect = new Rectangle((int)this.Origin.X, (int)this.Origin.Y, (int)stringSize.X, (int)stringSize.Y);
                if (mouseState.LeftButton == ButtonState.Released && overlayRect.Contains(mousePosition) && IsClickable == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            /// <summary>
            /// Check if a button is clicked
            /// </summary>
            /// <param name="mousePosition">The mouse position.</param>
            /// <param name="mouseState">State of the mouse.</param>
            /// <param name="oldMouseState">Old state of the mouse.</param>
            /// <param name="font">The font.</param>
            /// <returns></returns>
            public virtual bool chekIfCliked(Point mousePosition, MouseState mouseState, MouseState oldMouseState, SpriteFont font)
            {
                MouseState oldState = mouseState;
                var stringSize = font.MeasureString(this.TextButton);
                Rectangle overlayRect = new Rectangle((int)this.Origin.X, (int)this.Origin.Y, (int)stringSize.X, (int)stringSize.Y);
                if (mouseState.LeftButton == ButtonState.Pressed && overlayRect.Contains(mousePosition) && oldMouseState.LeftButton == ButtonState.Released)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}
