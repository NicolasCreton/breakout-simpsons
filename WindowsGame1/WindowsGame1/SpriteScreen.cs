﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using BreakOut;
using BreakOutModel;
using BreakOutScreen.ScreenButton;
using BreakOutResources;

namespace BreakOutScreen
{
    /// <summary>
    /// Base class to create new screen in the game
    /// </summary>
    public class SpriteScreen
    {

        public GameModel Model { get; set; }
        public Rectangle Location { get; set; }
        public Texture2D Texture { get; set; }
        public Color Tint { get; set; }
        public Texture2D CursorTex { get; set; }
        public Texture2D CursorTexBig { get; set; }
        public Vector2 CursorPos { get; set; }
        public bool MouseOver { get; set; }
        public List<ClickableButton> ButtonList { get; set; }
        public ClickableButton SelectedButton { get; set; }
        public int Spacing { get; set; }
        public SpriteFont TitleFont { get; set; }
        public SpriteFont NormalFont { get; set; }
        public SpriteFont SmallFont { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SpriteScreen"/> class.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="location">The location.</param>
        public SpriteScreen(GameModel model, Rectangle location)
        {
            this.Model = model;
            this.Location = location;
            this.ButtonList = new List<ClickableButton>();
            this.Tint = Color.White;
            this.MouseOver = false;
        }


        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public virtual void Initialize()
        {
            Spacing = 0;
        }


        /// <summary>
        /// Loads the content.
        /// </summary>
        /// Load the content of the screen like background and font of the different buttons
        /// <param name="content">The content</param>
        public virtual void LoadContent(ContentManager content)
        {
            this.TitleFont = Resources.LoadFont("fontsimplarge", content);
            this.NormalFont = Resources.LoadFont("fontsimp", content);
            this.SmallFont = Resources.LoadFont("fontsimpsmall", content);
            this.CursorTex = Resources.LoadImage("Pictures\\ball2_LowRes", content);
            this.CursorTexBig = Resources.LoadImage("Pictures\\Souris_LowRes", content);
        }

        /// <summary>
        /// Places the string of the button
        /// You have to choose the x and y divisor and the string will be placed thanks to this
        /// The spacing is the number of pixel separating two strings
        /// Uses generic, can be used with a list of ClickableButtons and ClickableButtonsLevel
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="buttons">The buttons.</param>
        /// <param name="spacing">The spacing.</param>
        /// <param name="xDivisor">The x divisor.</param>
        /// <param name="yDivisor">The y divisor.</param>
        public void placeString<T>(List<T> buttons, int spacing, float xDivisor, float yDivisor) where T : ClickableButton
        {
            foreach (T button in buttons)
            {
                button.placeFont(this.Spacing, xDivisor, yDivisor);
                this.Spacing += spacing;
            }
        }


        /// <summary>
        /// Draws the buttons in the screen thanks to his position and his scale
        ///Change the color of the button to white when the mouse is over him
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="buttons">The buttons.</param>
        /// <param name="batch">The batch.</param>
        /// <param name="scale">The scale.</param>
        public void drawButtons<T>(List<T> buttons, SpriteBatch batch, float scale) where T : ClickableButton
        {
            foreach (T button in buttons)
            {
                if (this.MouseOver)
                {
                    button.Draw(batch, scale, button.TextColor);
                    if (this.SelectedButton != null)
                    {
                        this.SelectedButton.Draw(batch, scale, Color.White);
                    }
                }
                else
                {
                    button.Draw(batch, scale, button.TextColor);
                }
            }
        }


        /// <summary>
        /// Checks if mouse is over the button, chage the boolean MouseOver if yes
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="buttonList">The button list.</param>
        /// <param name="mousePosition">The mouse position.</param>
        /// <param name="mouseState">State of the mouse.</param>
        public void checkIfMouseOver<T>(List<T> buttonList, Point mousePosition, MouseState mouseState) where T : ClickableButton
        {
            this.MouseOver = false;
            foreach (T button in buttonList)
            {
                if (button.checkIfMouseOver(mousePosition, mouseState, button.Font))
                {
                    this.MouseOver = true;
                    this.SelectedButton = button;
                }
            }
        }


        /// <summary>
        /// Updates the specified game time.
        /// This function updates the mouse position in the screen
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public virtual void Update(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            CursorPos = new Vector2(mouseState.X, mouseState.Y);
        }


        /// <summary>
        /// Handles the input in the screen, needs to be overrriden
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyBordSate">The old key bord sate.</param>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        public virtual void HandleInput(KeyboardState keyboardState, KeyboardState oldKeyBordSate, MouseState mouseState, MouseState oldMouseState)
        {
        }


        /// <summary>
        /// Draws the cursor in the screen
        /// The cursor chagnes when the mouse is over a ClickableButton
        /// </summary>
        /// <param name="spriteBatch">The sprite batch.</param>
        /// <param name="gameTime">The game time.</param>
        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (this.MouseOver == true)
            {
                spriteBatch.Draw(CursorTexBig, CursorPos, new Rectangle?(), Color.White, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 0f);
            }
            else
            {
                spriteBatch.Draw(CursorTex, CursorPos, new Rectangle?(), Color.White, 0f, new Vector2(0, 0), 0.55f, SpriteEffects.None, 0f);
            }
        }

        /// <summary>
        /// Updates the window title.
        /// </summary>
        public virtual void UpdateWindowTitle()
        {
            this.Model.Game.Window.Title = GameModel.GameTitle;
        }
    }
}
