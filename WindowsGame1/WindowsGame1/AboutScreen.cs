﻿using BreakOut;
using BreakOutModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BreakOutScreen.ScreenButton;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutResources;

namespace BreakOutScreen
{
    /// <summary>
    /// SubClass of SpriteScreen
    /// This is the About Screen
    /// </summary>
    public class AboutScreen : SpriteScreen
    {
        public ClickableButton Team { get; set; }
        public ClickableButton Collision { get; set; }
        public ClickableButton GameLogic { get; set; }
        public ClickableButton UIDesigner { get; set; }
        public ClickableButton ReturnToMenu { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="AboutScreen"/> class.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <param name="model">The model.</param>
        public AboutScreen(Rectangle location, GameModel model)
            : base(model, location)
        {
            this.Team = new ClickableButton("By D'oh ! Nut Team", false, Color.Brown);
            this.Collision = new ClickableButton("Collision management: Corentin Henry", false, Color.Black);
            this.GameLogic = new ClickableButton("Game logic : Clement Duployez & Celine Leplongeon", false, Color.Black);
            this.UIDesigner = new ClickableButton("UI Design and implementation : Nicolas Creton", false, Color.Black);
            this.ReturnToMenu = new ClickableButton("Return", true, Color.Brown);
            this.ButtonList.Add(this.Team);
            this.ButtonList.Add(this.Collision);
            this.ButtonList.Add(this.GameLogic);
            this.ButtonList.Add(this.UIDesigner);
            this.ButtonList.Add(this.ReturnToMenu);
        }

        /// <summary>
        /// Loads the content.
        /// </summary>
        /// <param name="content">The content</param>
        /// Load the content of the screen like background and font of the different buttons
        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
            this.Texture = Resources.LoadImage("Pictures\\backCredit",content);
            this.Team.Font = this.TitleFont;
            this.Collision.Font = this.SmallFont;
            this.GameLogic.Font = this.SmallFont;
            this.UIDesigner.Font = this.SmallFont;
            this.ReturnToMenu.Font = this.SmallFont;
            placeString(this.ButtonList, 100, 1.4f, 3);
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Handles the input in the screen for the buttons
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyBordSate">The old key bord sate.</param>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        public override void HandleInput(KeyboardState keyboardState, KeyboardState oldKeyBordSate, MouseState mouseState, MouseState oldMouseState)
        {
            var mousePosition = new Point(mouseState.X, mouseState.Y);
            if (keyboardState.IsKeyDown(Keys.Escape) && oldKeyBordSate.IsKeyUp(Keys.Escape) || this.ReturnToMenu.chekIfCliked(mousePosition, mouseState, oldMouseState, this.NormalFont))
            {
                this.Model.Game.SelectedScreen = this.Model.Game.SettingsScreen;
            }
            checkIfMouseOver(this.ButtonList, mousePosition, mouseState);
            base.HandleInput(keyboardState, oldKeyBordSate, mouseState, oldMouseState);
        }

        /// <summary>
        /// Updates the specified game time.
        /// This function updates the mouse position in the screen
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// Draws the cursor in the screen
        /// The cursor chagnes when the mouse is over a ClickableButton
        /// </summary>
        /// <param name="spriteBatch">The sprite batch.</param>
        /// <param name="gameTime">The game time.</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(this.Texture, this.Location, this.Tint);
            drawButtons(this.ButtonList, spriteBatch, 1f);
            base.Draw(spriteBatch, gameTime);
        }
    }
}
