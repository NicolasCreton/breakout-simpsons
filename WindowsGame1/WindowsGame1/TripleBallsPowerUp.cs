﻿using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutUtil;
using BreakOutItem.BreakOutPowerUpTags;

namespace BreakOutItem
{
    namespace BreakOutPowerUp
    {
        public class TripleBallPowerUp : PowerUp, BallPowerUp
        {
            private const string powerUpImageName = "PowerUp\\LowRes\\pu_3balls_LowRes";
            private const float internalZoom = 0.25f;

            public int NbBallsAfterPowerUp
            {
                get { return this.Model.Balls.Count * 3; }
            }

            public TripleBallPowerUp(GameModel model, Vector2 startPosition)
                : base(model, startPosition, powerUpImageName, internalZoom)
            {

            }

            /// <summary>
            /// Power Up Starting effect
            /// </summary>
            public override void LaunchPower()
            {
                LinkedList<Ball> balls = this.Model.Balls;
                this.Model.Balls = new LinkedList<Ball>();
                foreach (Ball ball in balls)
                {
                    ball.TripleBall();
                }
            }
        }
    }
}
