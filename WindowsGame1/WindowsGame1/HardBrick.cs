﻿using BreakOut;
using BreakOutItem.BreakOutBrick;
using BreakOutUtil;
using BreakOutModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutResources;

namespace BreakOutItem
{
    namespace BreakOutBrick
    {
        class HardBrick : Brick
        {
            private const float internalZoom = 0.28f;
            /// <summary>
            /// The secondary images to produce a 3-images brick.
            /// </summary>
            private Texture2D[] images = new Texture2D[3];

            /// <summary>
            /// Gets the symbol used to generate levels.
            /// </summary>
            /// <value>
            /// The symbol.
            /// </value>
            public static char Symbol
            {
                get { return 'x'; }
            }

            public const string BrickImageName = "bricks\\LowRes\\b_3lives_1_LowRes";

            /// <summary>
            /// Gets or sets the number of lives. Can be overriden to apply certain actions on life removal.
            /// </summary>
            /// <value>
            /// The lives. The image is updated according the number of images left.
            /// </value>
            public override int Lives
            {
                get
                {
                    return base.Lives;
                }
                set
                {
                    base.Lives = value;
                    if (base.Lives == 2)
                    {
                        this.Texture = images[1];
                    }
                    else if (base.Lives == 1)
                    {
                        this.Texture = images[2];
                    }
                    else if (base.Lives == 0)
                    {
                        this.Die();
                    }
                }
            }

            public HardBrick(GameModel model, Vector2 position, TabCoordinates coordinates)
                : base(model, position, coordinates, BrickImageName, null, internalZoom)
            {
                images[0] = this.Texture;
                images[1] = Resources.LoadImage("Pictures\\bricks\\LowRes\\b_3lives_2_LowRes", this.Model.Game);
                images[2] = Resources.LoadImage("Pictures\\bricks\\LowRes\\b_3lives_3_LowRes", this.Model.Game);  
                this.Solid = true;
                this.Lives = 3;
            }

            /// <summary>
            /// Removes a life everytime
            /// </summary>
            public override void GetHit()
            {
                this.Lives--;
            }
        }
    }
}