﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using BreakOut;
using BreakOutModel;
using BreakOutScreen.ScreenButton;
using BreakOutResources;

namespace BreakOutScreen
{
    /// <summary>
    /// SubClass of SpriteScreen
    /// This is the screen when you loose the game
    /// </summary>
    public class LooseScreen : SpriteScreen
    {
        public ClickableButton LooseButton { get; set; }
        public ClickableButton StartGameButton { get; set; }
        public ClickableButton MainMenuButton { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LooseScreen"/> class.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <param name="model">The model.</param>
        public LooseScreen(Rectangle location, GameModel model)
            : base(model, location)
        {
            this.LooseButton = new ClickableButton("HA HA ! YOU LOOSE !", false, Color.LightSalmon);
            this.StartGameButton = new ClickableButton("Restart (Enter)", true, Color.LightCyan);
            this.MainMenuButton = new ClickableButton("Main menu (Escape)", true, Color.LightCyan);
            this.ButtonList.Add(this.LooseButton);
            this.ButtonList.Add(this.StartGameButton);
            this.ButtonList.Add(this.MainMenuButton);
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Loads the content.
        /// </summary>
        /// <param name="content">The content</param>
        /// Load the content of the screen like background and font of the different buttons
        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
            this.Texture = Resources.LoadImage("Pictures\\backLoose2",content);
            this.LooseButton.Font = this.TitleFont;
            this.StartGameButton.Font = this.NormalFont;
            this.MainMenuButton.Font = this.NormalFont;
            placeString(this.ButtonList, 100, 4, 5);
        }


        /// <summary>
        /// Draws the specified spritebatch.
        /// </summary>
        /// <param name="spritebatch">The spritebatch.</param>
        /// <param name="gametime">The gametime.</param>
        public override void Draw(SpriteBatch spritebatch, GameTime gametime)
        {

            spritebatch.Draw(this.Texture, this.Location, this.Tint);
            drawButtons(this.ButtonList, spritebatch, 1f);

            base.Draw(spritebatch, gametime);
        }

        /// <summary>
        /// Handles the input of different buttons and change the actualScreen thanks to the clicked button
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyboardState">Old state of the keyboard.</param>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        public override void HandleInput(KeyboardState keyboardState, KeyboardState oldKeyboardState, MouseState mouseState, MouseState oldMouseState)
        {

            var mousePosition = new Point(mouseState.X, mouseState.Y);

            if (keyboardState.IsKeyDown(Keys.Escape) && oldKeyboardState.IsKeyUp(Keys.Escape)
                || MainMenuButton.chekIfCliked(mousePosition, mouseState, oldMouseState, NormalFont))
            {
                this.Model.Game.MenuScreen.PlayMusic();
                this.Model.Game.SelectedScreen = this.Model.Game.MenuScreen;
            }


            if (keyboardState.IsKeyDown(Keys.Enter) && oldKeyboardState.IsKeyUp(Keys.Enter)
                || StartGameButton.chekIfCliked(mousePosition, mouseState, oldMouseState, NormalFont))
            {
                this.Model.LoadModel(this.Model.actualLevelIndex);
                this.Model.ResetLevel();
                this.Model.Game.SelectedScreen = this.Model.Game.GameScreen;
                this.Model.Game.StartGame();
            }

            checkIfMouseOver(this.ButtonList, mousePosition, mouseState);
        }

        /// <summary>
        /// Updates the specified game time.
        /// This function updates the mouse position in the screen
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

    }
}
