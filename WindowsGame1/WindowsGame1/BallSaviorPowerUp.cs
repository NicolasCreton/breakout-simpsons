﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutModel;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using BreakOutUtil;

namespace BreakOutItem
{
    namespace BreakOutPowerUp
    {
        public class BallSaviorPowerUp : PowerUp
        {
            private const string powerUpImageName = "PowerUp\\LowRes\\pu_rope_LowRes";
            private const float internalZoom = 0.25f;

            public BallSaviorPowerUp(GameModel model, Vector2 startPosition)
                : base(model, startPosition, powerUpImageName, internalZoom)
            {
                this.Timer = 15000;
            }

            /// <summary>
            /// Power Up Starting effect
            /// </summary>
            public override void LaunchPower()
            {
                this.Model.Paddle.BallSavior = true;
            }

            /// <summary>
            /// Power Up Stop effect used when the power up is timed.
            /// </summary>
            public override void StopPower()
            {
                this.Model.Paddle.BallSavior = false;
            }

        }
    }
}

