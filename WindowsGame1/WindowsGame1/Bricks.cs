﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOut;
using BreakOutModel;
using Microsoft.Xna.Framework.Audio;
using BreakOutUtil;
using BreakOutItem.BreakOutItemTags;
using BreakOutResources;

namespace BreakOutItem
{
    namespace BreakOutBrick
    {

        /// <summary>
        /// Base of all bricks
        /// </summary>
        public abstract class Brick : CollidableItem, HittingItem
        {

            /// <summary>
            /// Gets or sets the number of lives. Can be overriden to apply certain actions on life removal.
            /// </summary>
            /// <value>
            /// The lives.
            /// </value>
            public virtual int Lives { get; set; }
            /// <summary>
            /// Sound played when the brick is hit.
            /// </summary>
            /// <value>
            /// The sound.
            /// </value>
            public SoundEffect Sound { get; set; }
            /// <summary>
            /// Gets or sets a value indicating whether this <see cref="Brick"/> is solid. If not, the ball can go through it (see GlassBrick)
            /// </summary>
            /// <value>
            ///   <c>true</c> if solid; otherwise, <c>false</c>.
            /// </value>
            public bool Solid { get; set; }
            private TabCoordinates coordinates;
            /// <summary>
            /// Gets the coordinates in the 2D Bricks array.
            /// </summary>
            /// <value>
            /// The coordinates.
            /// </value>
            public TabCoordinates Coordinates { get { return coordinates; } }

            public override void Update(GameTime gameTime)
            {
                //Nothing to be done here. Everything that happens to the brick depend on the other screen elements.
            }

            public Brick(GameModel model, Vector2 position, TabCoordinates coordinates, string brickImageName, string soundName, float internalZoom)
                : base(model, position, brickImageName, internalZoom, null)
            {
                if (soundName != null)
                {
                    this.Sound = Resources.LoadSoundEffect("Sounds\\" + soundName,this.Model.Game);
                }
                this.coordinates = coordinates;
            }

            /// <summary>
            /// Checks the collision. between this brick and a ball.
            /// </summary>
            /// <param name="ball">A ball.</param>
            public virtual void CheckCollision(Ball ball)
            {
                if (this != null && ball.Intersects(this))
                {
                    GetHit();
                    if (this.Solid)
                    {
                        this.Hit(ball);
                    }
                }
            }

            /// <summary>
            /// Hits the specified ball.
            /// </summary>
            /// <param name="ball">The ball.</param>
            public virtual void Hit(Ball ball)
            {
                Rectangle hitBox = ball.HitBox;
                Rectangle brickLocation = this.HitBox;
                this.PlayHitSound();

                if (hitBox.Center.X <= brickLocation.Left && ball.Speed.MovingDirection.X > 0)
                {
                    this.HitLeft(ball);
                }
                if (hitBox.Center.X >= brickLocation.Right && ball.Speed.MovingDirection.X < 0)
                {
                    this.HitRight(ball);
                }
                if (hitBox.Center.Y <= brickLocation.Top && ball.Speed.MovingDirection.Y > 0)
                {
                    this.HitTop(ball);
                }
                if (hitBox.Center.Y >= brickLocation.Bottom && ball.Speed.MovingDirection.Y < 0)
                {
                    this.HitBottom(ball);
                }
            }

            /// <summary>
            /// Play the hit sound
            /// </summary>
            public void PlayHitSound()
            {
                SoundEffect sound = this.Model.Game.GameScreen.BrickSound;
                if (sound != null)
                {
                    sound.Play();
                }
            }

            /// <summary>
            /// The ball hits the top of the brick
            /// </summary>
            /// <param name="ball">The ball.</param>
            public virtual void HitTop(Ball ball)
            {
                Rectangle hitBox = ball.HitBox;
                Rectangle brickLocation = this.HitBox;
                ball.moveTo(new Vector2(ball.X, this.Top - hitBox.Height));
                ball.Speed.MultiplyMovingDirection(1, -1);
                ball.LastCollision = CollisionType.Brick;
            }
            /// <summary>
            /// The ball hits the left side of the brick
            /// </summary>
            /// <param name="ball">The ball.</param>
            public virtual void HitLeft(Ball ball)
            {
                Rectangle hitBox = ball.HitBox;
                Rectangle brickLocation = this.HitBox;
                ball.moveTo(new Vector2(this.Left - hitBox.Width, ball.Y));
                ball.Speed.MultiplyMovingDirection(-1, 1);
                ball.LastCollision = CollisionType.Brick;
            }
            /// <summary>
            /// The ball hits the right side of the brick
            /// </summary>
            /// <param name="ball">The ball.</param>
            public virtual void HitRight(Ball ball)
            {
                Rectangle hitBox = ball.HitBox;
                Rectangle brickLocation = this.HitBox;
                ball.moveTo(new Vector2(this.Right + 1, ball.Y));
                ball.Speed.MultiplyMovingDirection(-1, 1);
                ball.LastCollision = CollisionType.Brick;
            }
            /// <summary>
            /// The ball hits the bottom of the brick.
            /// </summary>
            /// <param name="ball">The ball.</param>
            public virtual void HitBottom(Ball ball)
            {
                Rectangle hitBox = ball.HitBox;
                Rectangle brickLocation = this.HitBox;
                ball.moveTo(new Vector2(ball.X, this.Bottom + 1));
                ball.Speed.MultiplyMovingDirection(1, -1);
                ball.LastCollision = CollisionType.Brick;
            }

            /// <summary>
            /// Removes the brick.
            /// </summary>
            protected void Die()
            {
                TabCoordinates coordinates = this.Coordinates;
                this.Model.Level[coordinates.I, coordinates.J] = null;
                this.Model.AddRandomFallingPowerUp(this.Center);
            }

            public virtual void GetHit()
            {
                this.Die();
            }

            /// <summary>
            /// Gets the neighbors of this brick.
            /// </summary>
            /// <param name="currentCoordinates">The current coordinates.</param>
            /// <returns></returns>
            public Brick[] GetNeighbors(TabCoordinates currentCoordinates)
            {
                Brick[] neighbors = new Brick[8];
                int currentCoordI = currentCoordinates.I;
                int currentCoordJ = currentCoordinates.J;
                int nbRows = this.Model.Level.NbRows;
                int nbCols = this.Model.Level.NbColumns;
                int index = 0;
                for (int i = currentCoordI - 1; i <= currentCoordI + 1; i++)
                {
                    for (int j = currentCoordJ - 1; j <= currentCoordJ + 1; j++)
                    {
                        if (i != currentCoordI || j != currentCoordJ)
                        {
                            if (i >= 0 && j >= 0 && i < nbRows && j < nbCols)
                            {
                                neighbors[index++] = Model.Level[i, j];
                            }
                        }
                    }
                }
                return neighbors;
            }
        }
    }
}
