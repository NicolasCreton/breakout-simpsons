﻿using BreakOutItem;
using BreakOutLevel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOut;
using BreakOutUtil;
using BreakOutItem.BreakOutPowerUp;
using System.Reflection;
using System.Diagnostics;
using BreakOutItem.BreakOutPowerUpTags;
using BreakOutItem.BreakOutBrick;
using Microsoft.Xna.Framework.Audio;

namespace BreakOutModel
{
    public class GameModel
    {
        private List<Type> powerUpClasses;
        private const int powerUpProbability = 7;
        private Random randomizer = new Random();
        private const int ballLimit = 15;
        private LinkedList<PowerUp> fallingPowerUpsToRemove = new LinkedList<PowerUp>();
        private LinkedList<PowerUp> activePowerUpsToRemove = new LinkedList<PowerUp>();
        private LinkedList<Ball> ballsToRemove = new LinkedList<Ball>();

        /// <summary>
        /// The game title
        /// </summary>
        public const string GameTitle = "Homer's Break";
        public BreakOutGame Game { get; private set; }
        /// <summary>
        /// Gets or sets a value indicating whether [game won].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [game won]; otherwise, <c>false</c>.
        /// </value>
        public bool GameWon { get; set; }
        /// <summary>
        /// Gets or sets the level of bricks.
        /// </summary>
        /// <value>
        /// The level.
        /// </value>
        public Level Level { get; set; }
        /// <summary>
        /// Gets or sets the active power ups.
        /// </summary>
        /// <value>
        /// The active power ups.
        /// </value>
        public LinkedList<PowerUp> ActivePowerUps { get; set; }
        /// <summary>
        /// Gets or sets the user lives.
        /// </summary>
        /// <value>
        /// The lives.
        /// </value>
        public int Lives { get; set; }
        /// <summary>
        /// Gets or sets the balls : List of the balls on the game screen
        /// </summary>
        /// <value>
        /// The balls.
        /// </value>
        public LinkedList<Ball> Balls { get; set; }
        /// <summary>
        /// Gets or sets the paddle.
        /// </summary>
        /// <value>
        /// The paddle.
        /// </value>
        public Paddle Paddle { get; set; }
        /// <summary>
        /// Gets or sets the falling power ups which can be caught by the paddle.
        /// </summary>
        /// <value>
        /// The falling power ups.
        /// </value>
        public LinkedList<PowerUp> FallingPowerUps { get; set; }
        /// <summary>
        /// Gets or sets the level manager which permits to load levels
        /// </summary>
        /// <value>
        /// The level manager.
        /// </value>
        public LevelManager LevelManager { get; set; }
        /// <summary>
        /// Gets or sets the actual level index.
        /// </summary>
        /// <value>
        /// The actual level.
        /// </value>
        public int actualLevelIndex { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="GameModel"/> class.
        /// </summary>
        /// <param name="game">The game.</param>
        public GameModel(BreakOutGame game)
        {
            this.Game = game;
            this.Lives = 3;
            this.Balls = new LinkedList<Ball>();
            this.FallingPowerUps = new LinkedList<PowerUp>();
            this.ActivePowerUps = new LinkedList<PowerUp>();
            this.LevelManager = new LevelManager(this);
        }

        /// <summary>
        /// Loads the model.
        /// </summary>
        /// <param name="levelIndex">The level index.</param>
        public void LoadModel(int levelIndex)
        {
            this.Level = this.GetLevel(levelIndex);
            this.Level.LoadLevel();
            if (this.Paddle == null)
            {
                this.Paddle = new Paddle(this);
            }
            this.ListPowerUpClasses();
            GameWon = false;
        }

        /// <summary>
        /// Lists the power up classes.
        /// </summary>
        private void ListPowerUpClasses()
        {
            this.powerUpClasses = (AppDomain.CurrentDomain.GetAssemblies()
                       .SelectMany(t => t.GetTypes())
                       .Where(t => t.IsClass && !t.IsAbstract && t.IsSubclassOf(typeof(PowerUp)))).ToList<Type>();
        }

        /// <summary>
        /// Updates the active power ups.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        private void UpdateActivePowerUps(GameTime gameTime)
        {
            foreach (PowerUp powerUp in this.ActivePowerUps)
            {
                powerUp.Timer -= gameTime.ElapsedGameTime.TotalMilliseconds;
                if (powerUp is BallSaviorPowerUp && this.Paddle.BallSavior == false)
                {
                    this.RemoveActivePowerUp(powerUp);
                }
            }

            foreach (PowerUp powerUp in activePowerUpsToRemove)
            {
                ActivePowerUps.Remove(powerUp);
            }

        }

        /// <summary>
        /// Updates the falling power ups.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        private void UpdateFallingPowerUps(GameTime gameTime)
        {

            foreach (PowerUp powerUp in this.FallingPowerUps)
            {
                powerUp.Update(gameTime);
            }

            foreach (PowerUp powerUp in this.fallingPowerUpsToRemove)
            {
                this.FallingPowerUps.Remove(powerUp);
            }
        }

        /// <summary>
        /// Determines whether the game is won yet.
        /// </summary>
        /// <returns></returns>
        public bool IsWin()
        {
            bool gameWon = true;
            foreach (Brick brick in Level.Bricks)
            {
                if (brick != null && !(brick is UnbreakableBrick))
                {
                    gameWon = false;
                }
            }
            return gameWon;
        }


        /// <summary>
        /// Gets the level.
        /// </summary>
        /// <param name="levelNumber">The level number.</param>
        /// <returns></returns>
        private Level GetLevel(int levelNumber)
        {
            string[] fileNames = this.LevelManager.getListOfLevelFiles();
            Level level = this.LevelManager.getLevel(fileNames[levelNumber], this);
            return level;
        }

        /// <summary>
        /// Adds a random falling power up.
        /// </summary>
        /// <param name="center">The center.</param>
        public void AddRandomFallingPowerUp(Vector2 center)
        {
            if (powerUpClasses.Count > 0)
            {
                int random = randomizer.Next(powerUpProbability);
                if (random == 0)
                {
                    PowerUp powerUp = MakeNewPowerUpInstance(this.powerUpClasses[GetRandomPowerUpClassIndex()], new object[] { this, center });
                    if (powerUp is BallPowerUp && ((BallPowerUp)powerUp).NbBallsAfterPowerUp > ballLimit)
                    {
                        return;
                    }
                    this.FallingPowerUps.AddLast(powerUp);
                }
            }
        }


        /// <summary>
        /// Gets a random index of a power up class.
        /// </summary>
        /// <returns></returns>
        private int GetRandomPowerUpClassIndex()
        {
            return randomizer.Next(this.powerUpClasses.Count);
        }

        /// <summary>
        /// Makes a new power up instance.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="args">The arguments.</param>
        /// <returns></returns>
        private PowerUp MakeNewPowerUpInstance(Type type, object[] args)
        {
            return (PowerUp)Activator.CreateInstance(type, args);
        }

        /// <summary>
        /// Gets the random power up class.
        /// </summary>
        /// <returns></returns>
        private MemberInfo GetRandomPowerUpClass()
        {
            return this.powerUpClasses[0];
        }

        /// <summary>
        /// Destroys the specified power up.
        /// </summary>
        /// <param name="powerUp">The power up.</param>
        public void Destroy(PowerUp powerUp)
        {
            fallingPowerUpsToRemove.AddLast(powerUp);
        }

        /// <summary>
        /// Finds the active power up.
        /// </summary>
        /// <param name="powerUp">The power up.</param>
        /// <returns></returns>
        public PowerUp FindActivePowerUp(PowerUp powerUp)
        {
            foreach (PowerUp item in ActivePowerUps)
            {
                if (powerUp.GetType() == item.GetType())
                {
                    return powerUp;
                }
            }
            return null;
        }

        /// <summary>
        /// Resets the level.
        /// </summary>
        public void ResetLevel()
        {
            this.Lives = 3;
            this.Balls = new LinkedList<Ball>();
            this.Paddle = new Paddle(this);
            this.Balls.AddLast(new Ball(this));
            this.FallingPowerUps = new LinkedList<PowerUp>();
            this.ActivePowerUps = new LinkedList<PowerUp>();
            this.Paddle.ResetPaddle();
        }

        /// <summary>
        /// Resets the power up.
        /// </summary>
        public void ResetPowerUp()
        {
            foreach (PowerUp powerUp in this.ActivePowerUps)
            {
                powerUp.StopPower();
            }
            this.FallingPowerUps = new LinkedList<PowerUp>();
            this.ActivePowerUps = new LinkedList<PowerUp>();
        }

        /// <summary>
        /// Removes the active power up.
        /// </summary>
        /// <param name="powerUp">The power up.</param>
        public void RemoveActivePowerUp(PowerUp powerUp)
        {
            this.activePowerUpsToRemove.AddLast(powerUp);
        }

        /// <summary>
        /// Action performed when the game is won.
        /// </summary>
        public void Win()
        {
            SoundEffect sound = this.Game.GameScreen.WinSound;
            if (sound != null)
            {
                sound.Play();
            }
            this.Game.SelectedScreen = this.Game.WonScreen;
        }

        /// <summary>
        /// Checks if the game is lost.
        /// </summary>
        public void CheckLose()
        {
            if (NoBalls())
            {
                this.RemoveLife();
            }
        }

        /// <summary>
        /// Returns true if there are no more balls on screen.
        /// </summary>
        /// <returns></returns>
        private bool NoBalls()
        {
            return this.Balls.Count == 0;
        }

        /// <summary>
        /// Removes a life.
        /// </summary>
        public void RemoveLife()
        {
            this.Lives--;
            this.PrepareNewLifeGame();
            if (this.IsGameOver())
            {
                this.GameOver();
            }
            else
            {
                this.PrepareNewLifeGame();
            }
        }

        /// <summary>
        /// Prepares a new life game.
        /// </summary>
        private void PrepareNewLifeGame()
        {
            this.Balls.Clear();
            this.Balls.AddLast(new Ball(this));
            this.Paddle.ResetPaddle();
            this.ResetPowerUp();
            this.Paddle.IsLaunched = false;
        }

        /// <summary>
        /// Determines whether [is game over].
        /// </summary>
        /// <returns></returns>
        private bool IsGameOver()
        {
            return this.Lives <= 0;
        }

        /// <summary>
        /// Game Over - No more lives
        /// </summary>
        public void GameOver()
        {
            SoundEffect sound = this.Game.GameScreen.LooseSound;
            if (sound != null)
            {
                sound.Play();
            }
            this.Game.SelectedScreen = this.Game.LooseScreen;
        }

        /// <summary>
        /// Checks if the game is won
        /// </summary>
        public void CheckWin()
        {
            if (this.IsWin())
            {
                this.Win();
            }
        }

        /// <summary>
        /// Checks if the game is either won or lost.
        /// </summary>
        public void CheckWinOrLose()
        {
            this.CheckLose();
            this.CheckWin();
        }

        /// <summary>
        /// Updates the play items (powerups, paddle, balls)
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public void UpdatePlayItems(GameTime gameTime)
        {
            this.UpdatePowerUps(gameTime);
            this.UpdatePaddle(gameTime); // /!\ La mise à jour de la raquette doit se faire avant celle des balles afin de vérifier si les balles doivent être lancées (au clic)
            this.UpdateBalls(gameTime);
        }

        /// <summary>
        /// Updates the paddle.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        private void UpdatePaddle(GameTime gameTime)
        {
            this.Paddle.Update(gameTime);
        }

        /// <summary>
        /// Updates the balls.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        private void UpdateBalls(GameTime gameTime)
        {
            foreach (Ball ball in this.Balls)
            {
                ball.Update(gameTime);
            }
            this.ClearBallsToRemove();

        }

        /// <summary>
        /// Updates the power ups.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public void UpdatePowerUps(GameTime gameTime)
        {
            this.UpdateActivePowerUps(gameTime);
            this.UpdateFallingPowerUps(gameTime);
        }

        /// <summary>
        /// Removes a ball.
        /// </summary>
        /// <param name="ball">The ball.</param>
        public void RemoveBall(Ball ball)
        {
            ballsToRemove.AddLast(ball);
        }

        /// <summary>
        /// So as to avoid InvalidOperationException, balls to remove are stocked in this array and removed at the end of the game update.
        /// </summary>
        private void ClearBallsToRemove()
        {
            foreach (Ball ballToRemove in ballsToRemove)
            {
                this.Balls.Remove(ballToRemove);
            }
            ballsToRemove.Clear();
        }
    }
}