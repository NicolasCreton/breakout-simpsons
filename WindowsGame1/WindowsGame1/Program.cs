using BreakOutLevel;
using System;

namespace BreakOut
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (BreakOutGame game = new BreakOutGame())
            {
                game.Run();
            }
        }
    }
#endif
}

