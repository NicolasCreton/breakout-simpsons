﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOut;
using BreakOutModel;
using BreakOutItem.BreakOutPowerUpTags;
using BreakOutUtil;
using System.Diagnostics;
using BreakOutItem.BreakOutItemTags;
using Microsoft.Xna.Framework.Audio;

namespace BreakOutItem
{
    public class Paddle : CollidableItem, ResizableItem, HittingItem
    {
        private Random randomizer = new Random();
        private MouseState mouseState;
        private const string paddleImageName = "paddle2";
        private const float internalZoom = 0.15f;
        private const int maxDecrement = -3;
        private const int maxIncrement = 3;
        private const float stepIncrement = 0.2f;
        private Item ship;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Paddle"/> is sticky : Whether the ball should bounce or get stuck on the paddle.
        /// </summary>
        /// <value>
        ///   <c>true</c> if sticky; otherwise, <c>false</c>.
        /// </value>
        public bool Sticky { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Paddle"/> is mirror.
        /// </summary>
        /// <value>
        ///   <c>true</c> if mirror (The mouse X axis is inverted); otherwise, <c>false</c>.
        /// </value>
        public bool Mirror { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether [ball savior]. If one ball goes off bottom, the ball goes back up.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [ball savior]; otherwise, <c>false</c>.
        /// </value>
        public bool BallSavior { get; set; }
        public const int BottomMargin = -40;
        public bool IsLaunched { get; set; }

        public Paddle(GameModel model)
            : base(model, new Vector2(), paddleImageName, internalZoom, new PowerUpZoom(maxDecrement, maxIncrement, stepIncrement))
        {
            this.IsLaunched = false;
            this.Sticky = false;
            this.Mirror = false;
            this.BallSavior = false;
            this.ship = new FuturamaShip(this.Model);
        }

        /// <summary>
        /// Updates paddle position according to the mouse.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            mouseState = Mouse.GetState();
            if (!this.Mirror)
            {
                this.moveTo(mouseState.X, Position.Y);
            }
            else
            {
                this.moveTo(-mouseState.X + this.Model.Game.ScreenBounds.Width, Position.Y);
            }
            if (this.BallSavior)
            {
                ship.Update(gameTime);
            }
            LockPaddle();
        }

        /// <summary>
        /// Displays the paddle and the futurama ship is the Ball Savior powerup is enabled.
        /// </summary>
        /// <param name="spriteBatch">The sprite batch.</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            if (this.BallSavior)
            {
                ship.Draw(spriteBatch);
            }
            base.Draw(spriteBatch);
        }

        /// <summary>
        /// Makes sure the paddle doesn't go off limits on right or left walls.
        /// </summary>
        private void LockPaddle()
        {
            if (Position.X < 0)
            {
                this.moveTo(0, Position.Y);
            }
            if (Position.X + (Size.Width) > this.Model.Game.ScreenBounds.Width)
            {
                this.moveTo(this.Model.Game.ScreenBounds.Width - Size.Width, Position.Y);
            }
        }

        /// <summary>
        /// Sets the paddle in start position.
        /// </summary>
        public void SetInStartPosition()
        {
            this.Position = new Vector2((Model.Game.ScreenBounds.Width - Size.Width) / 2, (Model.Game.ScreenBounds.Height - Size.Height));
        }

        /// <summary>
        /// Launches the balls.
        /// </summary>
        public void LaunchBall()
        {
            this.IsLaunched = true;
        }

        /// <summary>
        /// Grows this instance.
        /// </summary>
        public void Grow()
        {
            this.Zoom.PowerUpZoom++;
        }

        /// <summary>
        /// Shrinks this instance.
        /// </summary>
        public void Shrink()
        {
            this.Zoom.PowerUpZoom--;
        }

        /// <summary>
        /// Resets the paddle to its original state.
        /// </summary>
        public void ResetPaddle()
        {
            this.Zoom.PowerUpZoom = new PowerUpZoom(maxDecrement, maxIncrement, stepIncrement);
            this.IsLaunched = false;
            this.Sticky = false;
            this.BallSavior = false;

        }

        /// <summary>
        /// Hits the specified ball.
        /// </summary>
        /// <param name="ball">The ball.</param>
        public virtual void Hit(Ball ball)
        {
            this.HitTop(ball);
            if (ball.LastCollision != CollisionType.Paddle)
            {
                this.PlayPaddleCollisionSound();
            }
        }
        /// <summary>
        /// Plays the paddle collision sound.
        /// </summary>
        private void PlayPaddleCollisionSound()
        {
            int size = this.Model.Game.GameScreen.PaddleSounds.Count;
            int index = randomizer.Next(0, size);
            SoundEffect sound = this.Model.Game.GameScreen.PaddleSounds[index];
            if (sound != null)
            {
                sound.Play();
            }
        }


        /// <summary>
        /// The ball hit the top of the paddle.
        /// </summary>
        /// <param name="ball">The ball.</param>
        public virtual void HitTop(Ball ball)
        {
            Rectangle paddleLocation = HitBox;
            // this vector will store reflection direction
            Vector2 normalVector = new Vector2();

            // relative position of the ball to the racket's center in percents of the racket's half width
            normalVector.X = 2.0f * (ball.Center.X - this.Center.X) / this.Size.Width;
            // arbitrary Y coordinate : the lower Y , the higher the angle difference between two collision points (next to each other)
            normalVector.Y = -0.40f;
            ball.Speed.MovingDirection = Vector2.Normalize(normalVector);
            ball.Speed += 0.001f;
            if (this.Sticky)
            {
                ball.CenterDifferenceWithPaddleCenter = new Vector2(ball.Center.X - this.Center.X, -(this.Size.Height + ball.Size.Height) / 2);
            }
        }

        /// <summary>
        /// Not yet implemented. The ball hits the left side of the paddle.
        /// </summary>
        /// <param name="ball">The ball.</param>
        /// <exception cref="NotImplementedException"></exception>
        public virtual void HitLeft(Ball ball)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        //  Not yet implemented. The ball hits the left side of the paddle.
        /// </summary>
        /// <param name="ball">The ball.</param>
        /// <exception cref="NotImplementedException"></exception>
        public virtual void HitRight(Ball ball)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Not yet implemented. The ball hits the bottom side of the paddle and needs to fall.
        /// </summary>
        /// <param name="ball">The ball.</param>
        /// <exception cref="NotImplementedException"></exception>
        public virtual void HitBottom(Ball ball)
        {
            throw new NotImplementedException();
        }
    }
}
