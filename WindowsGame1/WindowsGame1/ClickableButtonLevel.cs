﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace BreakOutScreen
{
    namespace ScreenButton
    {
        /// <summary>
        /// SubClass of ClickableButton
        /// It represents a ClickableButton but with a level number
        /// When the button is clicked the model knows which level to load thanks to the LevelNumber
        /// </summary>
        public class ClickableButtonLevel : ClickableButton
        {
            public int LevelNumber { get; set; }

            /// <summary>
            /// Initializes a new instance of the <see cref="ClickableButtonLevel"/> class.
            /// </summary>
            /// <param name="text">The text.</param>
            /// <param name="clickable">if set to <c>true</c> [clickable].</param>
            /// <param name="color">The color.</param>
            /// <param name="levelNumber">The level number.</param>
            public ClickableButtonLevel(string text, bool clickable, Color color, int levelNumber)
                : base(text, clickable, color)
            {
                this.LevelNumber = levelNumber;
            }

            /// <summary>
            /// Places the font of the string thanks to the spacing and the x and y divisor
            /// The strings are placed thanks to the screen resolutions to be always visible
            /// </summary>
            /// <param name="ySpacing">The y spacing.</param>
            /// <param name="xDivisor">The x divisor.</param>
            /// <param name="yDivisor">The y divisor.</param>
            public override void placeFont(int ySpacing, float xDivisor, float yDivisor)
            {
                base.placeFont(ySpacing, xDivisor, yDivisor);
            }

            /// <summary>
            /// Checks if mouse over.
            /// </summary>
            /// <param name="mousePosition">The mouse position.</param>
            /// <param name="mouseState">State of the mouse.</param>
            /// <param name="font">The font.</param>
            /// <returns></returns>
            public override bool checkIfMouseOver(Point mousePosition, MouseState mouseState, SpriteFont font)
            {
                return base.checkIfMouseOver(mousePosition, mouseState, font);
            }

            /// <summary>
            /// Check if a button is clicked
            /// </summary>
            /// <param name="mousePosition">The mouse position.</param>
            /// <param name="mouseState">State of the mouse.</param>
            /// <param name="oldMouseState">Old state of the mouse.</param>
            /// <param name="font">The font.</param>
            /// <returns></returns>
            public override bool chekIfCliked(Point mousePosition, MouseState mouseState, MouseState oldMouseState, SpriteFont font)
            {
                return base.chekIfCliked(mousePosition, mouseState, oldMouseState, font);
            }

            /// <summary>
            /// Draws the buttons with their colors and scale
            /// </summary>
            /// <param name="batch">The batch.</param>
            /// <param name="scale">The scale.</param>
            /// <param name="textColor">Color of the text.</param>
            public override void Draw(SpriteBatch batch, float scale, Color textColor)
            {
                base.Draw(batch, scale, textColor);
            }
        }
    }
}
