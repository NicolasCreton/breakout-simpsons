﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using BreakOut;
using BreakOutModel;
using Microsoft.Xna.Framework.Media;
using BreakOutScreen.ScreenButton;
using BreakOutResources;

namespace BreakOutScreen
{
    /// <summary>
    /// Subclass of the SpriteScreen
    /// This is the screen when you win the game
    /// You can start a new game
    /// or go to the main menu
    /// </summary>
    public class WonScreen : SpriteScreen
    {
        public ClickableButton WonButton { get; set; }
        public ClickableButton StartGameButton { get; set; }
        public ClickableButton MainMenuButton { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="WonScreen"/> class.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <param name="model">The model.</param>
        public WonScreen(Rectangle location, GameModel model)
            : base(model, location)
        {
            this.WonButton = new ClickableButton("WHOOHOO !", false, Color.Brown);
            this.StartGameButton = new ClickableButton("Start a new game (Enter)", true, Color.Black);
            this.MainMenuButton = new ClickableButton("Main menu (Escape)", true, Color.Black);
            this.ButtonList.Add(this.WonButton);
            this.ButtonList.Add(this.StartGameButton);
            this.ButtonList.Add(this.MainMenuButton);
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Loads the content.
        /// </summary>
        /// <param name="content">The content</param>
        /// Load the content of the screen like background and font of the different buttons
        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
            this.Texture = Resources.LoadImage("Pictures\\backWon2", content);
            this.WonButton.Font = this.TitleFont;
            this.StartGameButton.Font = this.NormalFont;
            this.MainMenuButton.Font = this.NormalFont;
            placeString(this.ButtonList, 100, 5, 3);
        }


        /// <summary>
        /// Draws the specified spritebatch.
        /// </summary>
        /// <param name="spritebatch">The spritebatch.</param>
        /// <param name="gametime">The gametime.</param>
        public override void Draw(SpriteBatch spritebatch, GameTime gametime)
        {

            spritebatch.Draw(this.Texture, this.Location, this.Tint);
            drawButtons(this.ButtonList, spritebatch, 1f);

            base.Draw(spritebatch, gametime);
        }

        /// <summary>
        /// Handles the input.
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyboardState">Old state of the keyboard.</param>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        public override void HandleInput(KeyboardState keyboardState, KeyboardState oldKeyboardState, MouseState mouseState, MouseState oldMouseState)
        {

            var mousePosition = new Point(mouseState.X, mouseState.Y);

            if (keyboardState.IsKeyDown(Keys.Escape) && oldKeyboardState.IsKeyUp(Keys.Escape)
                || MainMenuButton.chekIfCliked(mousePosition, mouseState, oldMouseState, NormalFont))
            {
                this.Model.Game.MenuScreen.PlayMusic();
                this.Model.Game.SelectedScreen = this.Model.Game.MenuScreen;
            }

            if (keyboardState.IsKeyDown(Keys.Enter) && oldKeyboardState.IsKeyUp(Keys.Enter)
                || StartGameButton.chekIfCliked(mousePosition, mouseState, oldMouseState, NormalFont))
            {
                MediaPlayer.Play(this.Model.Game.MenuTheme);
                this.Model.Game.SelectedScreen = this.Model.Game.LevelScreen;
            }


            checkIfMouseOver(this.ButtonList, mousePosition, mouseState);

        }

        /// <summary>
        /// Updates the specified game time.
        /// This function updates the mouse position in the screen
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

    }
}
