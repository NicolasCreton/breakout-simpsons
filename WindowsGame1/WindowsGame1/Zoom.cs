﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutModel;

namespace BreakOutUtil
{
    public class Zoom
    {
        private static float globalZoom = 2.5f * GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width / 1920.0f;
        /// <summary>
        /// Gets or sets the global zoom, common to all items
        /// </summary>
        /// <value>
        /// The global zoom.
        /// </value>
        public static float GlobalZoom
        {
            get
            {
                return globalZoom;
            }
            set
            {
                globalZoom = value;
            }
        }

        private int bricksNumberWidth;
        private int bricksNumberHeight;
        private float secondInternalZoom = 1.5f;
        /// <summary>
        /// Gets or sets the second internal zoom used to adapt to the user screen size.
        /// </summary>
        /// <value>
        /// The second internal zoom.
        /// </value>
        public float SecondInternalZoom
        {
            get
            {
                return secondInternalZoom;
            }
            set
            {
                secondInternalZoom = value;
            }
        }
        /// <summary>
        /// Gets or sets the internal zoom used by the power ups.
        /// </summary>
        /// <value>
        /// The internal zoom.
        /// </value>
        public float InternalZoom { get; set; }
        public PowerUpZoom PowerUpZoom { get; set; }

        /// <summary>
        /// Gets the game zoom. (InternalZoom * PowerUpZoom)
        /// </summary>
        /// <value>
        /// The game zoom.
        /// </value>
        public float GameZoom
        {
            get
            {
                float result = InternalZoom * GlobalZoom;
                if (PowerUpZoom != null)
                {
                    result *= PowerUpZoom.Zoom;
                }
                return result;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Zoom"/> class.
        /// </summary>
        /// <param name="internalZoom">The internal zoom.</param>
        /// <param name="powerUpZoom">The power up zoom.</param>
        /// <param name="Model">The model.</param>
        public Zoom(float internalZoom, PowerUpZoom powerUpZoom, GameModel Model)
        {
            if (Model.Level != null)
            {
                this.bricksNumberWidth = Model.Level.NbColumns;
                this.bricksNumberHeight = Model.Level.NbRows;
                SecondInternalZoom = 1.5f * 10 / Math.Max(this.bricksNumberWidth, 3 * this.bricksNumberHeight);
            }
            this.InternalZoom = internalZoom * SecondInternalZoom;
            this.PowerUpZoom = powerUpZoom;
        }
    }
}
