﻿using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutUtil;
using System.Diagnostics;
using BreakOutItem.BreakOutPowerUpTags;

namespace BreakOutItem
{
    namespace BreakOutPowerUp
    {
        public class NineMiniBallsPowerUp : PowerUp, BallPowerUp
        {
            private const string powerUpImageName = "PowerUp\\pu_9balls";
            private const float internalZoom = 0.1f;

            public int NbBallsAfterPowerUp
            {
                get { return this.Model.Balls.Count - 1 + 9; }
            }

            public NineMiniBallsPowerUp(GameModel model, Vector2 startPosition)
                : base(model, startPosition, powerUpImageName, internalZoom)
            {

            }

            /// <summary>
            /// Power Up Starting effect
            /// </summary>
            public override void LaunchPower()
            {
                Ball ball = this.Model.Balls.First();
                this.Model.Balls.Remove(ball);
                LinkedList<Ball> balls = ball.MultiplyNbBallsBy(9);
                foreach (Ball newBall in balls)
                {
                    newBall.Zoom.PowerUpZoom.SetToMinimumSize();
                }
            }
        }
    }
}
