﻿using BreakOut;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutResources
{
    /// <summary>
    /// Permits to load one content only once
    /// </summary>
    public class Resources
    {
        private static Dictionary<string, Texture2D> images = new Dictionary<string, Texture2D>();
        private static Dictionary<string, Song> songs = new Dictionary<string, Song>();
        private static Dictionary<string, SoundEffect> soundEffects = new Dictionary<string, SoundEffect>();
        private static Dictionary<string, SpriteFont> fonts = new Dictionary<string, SpriteFont>();

        /// <summary>
        /// Loads the image.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="game">The game.</param>
        /// <returns></returns>
        public static Texture2D LoadImage(string filename, BreakOutGame game)
        {
            return LoadImage(filename, game.Content);
        }

        /// <summary>
        /// Loads the image.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="content">The content.</param>
        /// <returns></returns>
        public static Texture2D LoadImage(string filename, ContentManager content)
        {
            try
            {
                return images[filename];
            }
            catch (KeyNotFoundException)
            {
                images[filename] = content.Load<Texture2D>(filename);
                return images[filename];
            }
        }

        /// <summary>
        /// Loads the song.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="game">The game.</param>
        /// <returns></returns>
        public static Song LoadSong(string filename, BreakOutGame game)
        {
            return LoadSong(filename, game.Content);
        }

        /// <summary>
        /// Loads the song.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="content">The content.</param>
        /// <returns></returns>
        public static Song LoadSong(string filename, ContentManager content)
        {
            try
            {
                return songs[filename];
            }
            catch (KeyNotFoundException)
            {
                try
                {
                    songs[filename] = content.Load<Song>(filename);
                    return songs[filename];
                }
                catch
                {
                    
                }
            }
            return null;
        }

        /// <summary>
        /// Loads the sound effect.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="game">The game.</param>
        /// <returns></returns>
        public static SoundEffect LoadSoundEffect(string filename, BreakOutGame game)
        {
            return LoadSoundEffect(filename, game.Content);
        }

        /// <summary>
        /// Loads the sound effect.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="content">The content.</param>
        /// <returns></returns>
        public static SoundEffect LoadSoundEffect(string filename, ContentManager content)
        {
            try
            {
                return soundEffects[filename];
            }
            catch (KeyNotFoundException)
            {
                try
                {
                    soundEffects[filename] = content.Load<SoundEffect>(filename);
                    return soundEffects[filename];
                }
                catch
                {

                }
            }
            return null;
        }

        /// <summary>
        /// Loads the font.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="game">The game.</param>
        /// <returns></returns>
        public static SpriteFont LoadFont(string filename, BreakOutGame game)
        {
            return LoadFont(filename, game.Content);
        }

        /// <summary>
        /// Loads the font.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <param name="content">The content.</param>
        /// <returns></returns>
        public static SpriteFont LoadFont(string filename, ContentManager content)
        {
            try
            {
                return fonts[filename];
            }
            catch (KeyNotFoundException)
            {
                fonts[filename] = content.Load<SpriteFont>(filename);
                return fonts[filename];
            }
        }
    }
}
