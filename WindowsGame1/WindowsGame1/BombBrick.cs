﻿using BreakOut;
using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutUtil;

namespace BreakOutItem
{
    namespace BreakOutBrick
    {
        class BombBrick : Brick
        {
            private const float internalZoom = 0.28f;

            public const string BrickImageName = "bricks\\LowRes\\b_bomb_LowRes";
            public const string SoundName = "bomb";

            /// <summary>
            /// Gets the symbol used to generate levels.
            /// </summary>
            /// <value>
            /// The symbol.
            /// </value>
            public static char Symbol
            {
                get
                {
                    return '@';
                }
            }

            public BombBrick(GameModel model, Vector2 position, TabCoordinates coordinates)
                : base(model, position, coordinates, BrickImageName, SoundName, internalZoom)
            {
                this.Solid = true;
                this.Lives = 1;
            }

            /// <summary>
            /// Defines the effect when the brick gets hit
            /// </summary>
            public override void GetHit()
            {
                if (this.Sound != null)
                {
                    this.Sound.Play();
                }
                this.Die();
                this.Explode(this.Coordinates);
            }

            /// <summary>
            /// Explodes the neighbors in the bricks array
            /// </summary>
            /// <param name="arrayCoordinates">The coordinates.</param>
            public void Explode(TabCoordinates arrayCoordinates)
            {
                Brick[] bricks = GetNeighbors(arrayCoordinates);
                foreach (Brick brick in bricks)
                {
                    if (brick != null)
                    {
                        brick.GetHit();
                    }
                }
            }

            
        }
    }
}