﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using BreakOut;
using BreakOutModel;
using BreakOutScreen.ScreenButton;
using BreakOutResources;

namespace BreakOutScreen
{
    /// <summary>
    /// Subclass of the SpriteScreen
    /// This is the pause screen where you can go to the menu or resume
    /// </summary>
    public class PauseScreen : SpriteScreen
    {
        public ClickableButton GamePaused { get; set; }
        public ClickableButton ResumeButton { get; set; }
        public ClickableButton MainMenuButton { get; set; }

        public PauseScreen(Rectangle location, GameModel model)
            : base(model, location)
        {
            this.GamePaused = new ClickableButton("PAUSED GAME", false, Color.Brown);
            this.ResumeButton = new ClickableButton("Resume game (Space)", true, Color.Black);
            this.MainMenuButton = new ClickableButton("Main menu (Escape)", true, Color.Black);
            this.ButtonList.Add(this.GamePaused);
            this.ButtonList.Add(this.ResumeButton);
            this.ButtonList.Add(this.MainMenuButton);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
            this.Texture = Resources.LoadImage("Pictures\\backStart2", content);
            this.GamePaused.Font = this.TitleFont;
            this.ResumeButton.Font = this.NormalFont;
            this.MainMenuButton.Font = this.NormalFont;
            placeString(this.ButtonList, 120, 4, 3);
        }


        public override void Draw(SpriteBatch spritebatch, GameTime gametime)
        {

            spritebatch.Draw(this.Texture, this.Location, this.Tint);
            drawButtons(this.ButtonList, spritebatch, 1f);

            base.Draw(spritebatch, gametime);
        }

        public override void HandleInput(KeyboardState keyboardState, KeyboardState oldKeyboardState, MouseState mouseState, MouseState oldMouseState)
        {
            var mousePosition = new Point(mouseState.X, mouseState.Y);

            if (keyboardState.IsKeyDown(Keys.Escape) && oldKeyboardState.IsKeyUp(Keys.Escape)
                || MainMenuButton.chekIfCliked(mousePosition, mouseState, oldMouseState, NormalFont))
            {
                this.Model.ResetLevel();
                this.Model.Game.MenuScreen.PlayMusic();
                this.Model.Game.SelectedScreen = this.Model.Game.MenuScreen;
            }


            if (this.ResumeButton.chekIfCliked(mousePosition, mouseState, oldMouseState, this.NormalFont))
            {
                this.Model.Game.SelectedScreen = this.Model.Game.GameScreen;
            }

            checkIfMouseOver(this.ButtonList, mousePosition, mouseState);
        }

        public void togglePause(KeyboardState keyboardState, KeyboardState oldKeyboardState)
        {
            if (keyboardState.IsKeyDown(Keys.Space) && oldKeyboardState.IsKeyUp(Keys.Space) || keyboardState.IsKeyDown(Keys.Escape) && oldKeyboardState.IsKeyUp(Keys.Escape))
            {
                if (this.Model.Game.SelectedScreen == this.Model.Game.PauseScreen)
                {
                    this.Model.Game.SelectedScreen = this.Model.Game.GameScreen;
                }
                else
                {
                    this.Model.Game.SelectedScreen = this.Model.Game.PauseScreen;
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void UpdateWindowTitle()
        {
            this.Model.Game.Window.Title = string.Format("{0} ({1}) - {2}", this.Model.Level.Name, "Paused", GameModel.GameTitle);
        }

    }
}
