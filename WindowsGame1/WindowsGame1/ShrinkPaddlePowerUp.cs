﻿using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutUtil;

namespace BreakOutItem
{
    namespace BreakOutPowerUp
    {
        public class ShrinkPaddlePowerUp : PowerUp
        {
            private const string powerUpImageName = "PowerUp\\LowRes\\pu_shrinkPaddle_LowRes";
            private const float internalZoom = 0.25f;

            public ShrinkPaddlePowerUp(GameModel model, Vector2 startPosition)
                : base(model, startPosition, powerUpImageName, internalZoom)
            {

            }

            /// <summary>
            /// Power Up Starting effect
            /// </summary>
            public override void LaunchPower()
            {
                this.Model.Paddle.Shrink();
            }
        }
    }
}
