﻿using BreakOutItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutUtil;
using BreakOutModel;
using BreakOutItem.BreakOutBrick;
using System.Collections;

namespace BreakOutLevel
{
    public class Level
    {
        private GameModel model;


        /// <summary>
        /// The symbol 2D-Array of this level, obtained from the txt file.
        /// </summary>
        private char[,] symbols;

        /// <summary>
        /// Gets or sets the number of horizontal bricks.
        /// </summary>
        /// <value>
        /// The nb horizontal bricks.
        /// </value>
        public int NbHorizontalBricks { get; set; }
        /// <summary>
        /// Gets or sets the number vertical bricks.
        /// </summary>
        /// <value>
        /// The nb vertical bricks.
        /// </value>
        public int NbVerticalBricks { get; set; }


        /// <summary>
        /// Gets or sets the bricks 2D-Array.
        /// </summary>
        /// <value>
        /// The bricks.
        /// </value>
        public Brick[,] Bricks { get; set; }

        /// <summary>
        /// Indexer : Shortcut from Bricks[i,j]
        /// </summary>
        /// <value>
        /// The <see cref="Brick"/>.
        /// </value>
        /// <param name="i">The i.</param>
        /// <param name="j">The j.</param>
        /// <returns></returns>
        public Brick this[int i, int j]
        {

            get { return this.Bricks[i, j]; }
            set { this.Bricks[i, j] = value; }
        }

        private int nbRows;

        /// <summary>
        /// Gets or sets the number of rows.
        /// </summary>
        /// <value>
        /// The nb rows.
        /// </value>
        public int NbRows
        {
            get { return nbRows; }
            set { nbRows = value; }
        }

        private int nbColumns;

        /// <summary>
        /// Gets or sets the number of columns.
        /// </summary>
        /// <value>
        /// The nb columns.
        /// </value>
        public int NbColumns
        {
            get { return nbColumns; }
            set { nbColumns = value; }
        }

        /// <summary>
        /// Gets or sets the name of the level.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Level"/> class. /!\ The Bricks 2D-Array is not loaded automatically and must be loaded with the LoadLevel() method.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="name">The name.</param>
        /// <param name="bricksSymbols">The bricks symbols.</param>
        public Level(GameModel model, string name, char[,] bricksSymbols)
        {
            this.model = model;
            this.Name = name;
            this.NbRows = bricksSymbols.GetLength(0);
            this.NbColumns = bricksSymbols.GetLength(1);
            this.symbols = bricksSymbols;
            this.Bricks = null;
        }

        /// <summary>
        /// Loads the level. Converts the symbols 2D-Array to a Brick 2D-Array.
        /// </summary>
        public void LoadLevel()
        {
            this.Bricks = this.model.LevelManager.toBricks(symbols, model);
        }


        /// <summary>
        /// Finds the specified brick.
        /// </summary>
        /// <param name="brick">The brick.</param>
        /// <returns></returns>
        public TabCoordinates Find(Brick brick)
        {
            for (int i = 0; i < Bricks.GetLength(0); i++)
            {
                for (int j = 0; j < Bricks.GetLength(1); j++)
                {
                    if (brick.Equals(Bricks[i, j]))
                    {
                        return new TabCoordinates(i, j);
                    }
                }
            }
            return null;
        }
    }
}
