﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutItem
{
    namespace BreakOutPowerUpTags
    {
        /// <summary>
        /// Permits to check if the ammount of balls after catching this power up may cause to game to lag, according to the ball limit set in the model.
        /// </summary>
        public interface BallPowerUp
        {
            int NbBallsAfterPowerUp { get; }
        }
    }
}
