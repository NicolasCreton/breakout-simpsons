﻿using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutUtil;

namespace BreakOutItem
{
    namespace BreakOutPowerUp
    {
        public class AccelerateBallPowerUp : PowerUp
        {
            private const string powerUpImageName = "PowerUp\\LowRes\\pu_ballSpeedUp_LowRes";
            private const float internalZoom = 0.25f;

            public AccelerateBallPowerUp(GameModel model, Vector2 startPosition)
                : base(model, startPosition, powerUpImageName, internalZoom)
            {

            }

            /// <summary>
            /// Power Up Starting effect
            /// </summary>
            public override void LaunchPower()
            {
                foreach (Ball ball in this.Model.Balls)
                {
                    ball.Accelerate();
                }
            }
        }
    }
}
