﻿using BreakOut;
using BreakOutUtil;
using BreakOutModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace BreakOutItem
{
    namespace BreakOutPowerUp
    {
        public abstract class PowerUp : FallingItem
        {
            private const float powerUpSpeed = 0.35f;
            private double? timer;

            /// <summary>
            /// Gets or sets the timer. If used, the power up will have a start time and a stop time.
            /// </summary>
            /// <value>
            /// The timer.
            /// </value>
            public double? Timer
            {
                get { return timer; }
                set
                {
                    timer = value;
                    if (timer <= 0)
                    {
                        StopPower();
                        RemoveFromActivePowerUps();
                    }
                }
            }

            public PowerUp(GameModel model, Vector2 startPosition, string powerUpImageName, float internalZoom, float speed = powerUpSpeed)
                : base(model, startPosition, powerUpImageName, new Speed(model, powerUpSpeed, new Vector2(0, 1)), internalZoom, null)
            {
                this.Model = model;
                this.setStartMovingDirection();
            }

            /// <summary>
            /// The power up falls from top to bottom
            /// </summary>
            private void setStartMovingDirection()
            {
                this.Speed.MovingDirection = new Vector2(0, 1);
            }

            /// <summary>
            /// Updates the item to its next state according to the specified game time.
            /// </summary>
            /// <param name="gameTime">The game time.</param>
            public override void Update(GameTime gameTime)
            {
                this.Step(gameTime);
                this.checkCollision();

                if (OffBottom())
                {
                    Model.Destroy(this);
                }
            }

            /// <summary>
            /// Checks the collision with other items which can catch this power up.
            /// </summary>
            private void checkCollision()
            {
                this.checkPaddleCollision();
            }

            /// <summary>
            /// Checks the paddle collision.
            /// </summary>
            private void checkPaddleCollision()
            {
                this.CheckPaddleCollision();
            }

            /// <summary>
            /// HThe powerup hits the paddle.
            /// </summary>
            public override void HitPaddle()
            {
                Model.Destroy(this);
                this.EnablePower();
            }

            /// <summary>
            /// Resets the active power up if already active. If this power up was already caught by the paddle and is still active, we put the timer back to its original state to make it last longer.
            /// </summary>
            /// <returns></returns>
            private bool resetActivePowerUpIfAlreadyActive()
            {
                List<PowerUp> powerUps = this.Model.ActivePowerUps.Where(item => item.GetType() == this.GetType()).ToList();
                if (powerUps.Count > 0)
                {
                    powerUps[0].Timer = this.Timer;
                    return true;
                }
                return false;
            }

            /// <summary>
            /// Enables the power of this power up.
            /// </summary>
            public void EnablePower()
            {
                if (this.Timer != null)
                {
                    if (!this.resetActivePowerUpIfAlreadyActive()) //If there's already this timed power up, reset the timer of the already active one.
                    {
                        this.Model.ActivePowerUps.AddLast(this);
                        this.LaunchPower();
                    }
                }
                else
                {
                    this.LaunchPower();
                }

            }

            /// <summary>
            /// Power Up Starting effect
            /// </summary>
            public abstract void LaunchPower();

            /// <summary>
            /// Power Up Stop effect used when the power up is timed.
            /// </summary>
            public virtual void StopPower()
            {

            }

            public virtual void ResetTimer()
            {

            }

            /// <summary>
            /// Removes from active power ups.
            /// </summary>
            public void RemoveFromActivePowerUps()
            {
                this.Model.RemoveActivePowerUp(this);
            }
        }
    }
}
