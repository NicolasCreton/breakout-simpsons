﻿using BreakOut;
using BreakOutItem.BreakOutBrick;
using BreakOutUtil;
using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutItem
{
    namespace BreakOutBrick
    {
        class GlassBrick : Brick
        {
            private const float internalZoom = 0.26f;

            /// <summary>
            /// Gets the symbol used to generate levels.
            /// </summary>
            /// <value>
            /// The symbol.
            /// </value>
            public static char Symbol
            {
                get { return '_'; }
            }

            public const string BrickImageName = "bricks\\LowRes\\b_glass_LowRes";
            public const string SoundName = "glassSound";

            public GlassBrick(GameModel model, Vector2 position, TabCoordinates coordinates)
                : base(model, position, coordinates, BrickImageName, SoundName, internalZoom)
            {
                this.Solid = false; //The brick lets the ball go through
                this.Lives = 1;
            }

            /// <summary>
            /// Action produced when the ball hits the brick
            /// </summary>
            public override void GetHit()
            {
                if (this.Sound != null)
                {
                    this.Sound.Play();
                }
                this.Die();
            }
        }
    }
}