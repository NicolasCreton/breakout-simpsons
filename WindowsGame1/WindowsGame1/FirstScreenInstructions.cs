﻿using BreakOutModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using BreakOutScreen.ScreenButton;
using BreakOutResources;
namespace BreakOutScreen
{
    /// <summary>
    /// Subclass of the SpriteScreen
    /// This is the first instruction screen
    /// Is only contains a link to the second one and a return buttons
    /// </summary>
    public class FirstScreenInstructions : SpriteScreen
    {
        public ClickableButton SecondInstruction { get; set; }
        public ClickableButton ReturnToMenu { get; set; }

        public FirstScreenInstructions(Rectangle location, GameModel model)
            : base(model, location)
        {
            this.ButtonList.Add(this.SecondInstruction = new ClickableButton("Next", true, Color.Black));
            this.ButtonList.Add(this.ReturnToMenu = new ClickableButton("Return", true, Color.Black));
            this.ButtonList.Add(this.ReturnToMenu);
        }

        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
            this.Texture = Resources.LoadImage("Pictures\\backInstruction",content);
            foreach (ClickableButton button in ButtonList)
            {
                button.Font = this.NormalFont;
            }
            placeString(this.ButtonList, 30,1.1f, 1.2f);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void HandleInput(KeyboardState keyboardState, KeyboardState oldKeyBordSate, MouseState mouseState, MouseState oldMouseState)
        {
            var mousePosition = new Point(mouseState.X, mouseState.Y);
            if (keyboardState.IsKeyDown(Keys.Escape) && oldKeyBordSate.IsKeyUp(Keys.Escape) || this.ReturnToMenu.chekIfCliked(mousePosition, mouseState, oldMouseState, this.NormalFont))
            {
                this.Model.Game.SelectedScreen = this.Model.Game.MenuScreen;
            }
            if (keyboardState.IsKeyDown(Keys.Enter) && oldKeyBordSate.IsKeyUp(Keys.Enter) || this.SecondInstruction.chekIfCliked(mousePosition, mouseState, oldMouseState, this.NormalFont))
            {
                this.Model.Game.SelectedScreen = this.Model.Game.SecondInstructionScreen;
            }
            checkIfMouseOver(this.ButtonList, mousePosition, mouseState);
            base.HandleInput(keyboardState, oldKeyBordSate, mouseState, oldMouseState);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(this.Texture, this.Location, this.Tint);
            drawButtons(this.ButtonList, spriteBatch, 1f);
            base.Draw(spriteBatch, gameTime);
        }
    }
}
