﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutUtil;
using BreakOut;
using BreakOutModel;
using System.Diagnostics;
using BreakOutResources;

namespace BreakOutItem
{
    /// <summary>
    /// Base class of all game elements
    /// </summary>
    public abstract class Item
    {
        private const string imagePath = "Pictures \\";
        private Vector2 position;

        /// <summary>
        /// Gets or sets the zoom.
        /// </summary>
        /// <value>
        /// Contains the global zoom / internal zoom / power up zoom information.
        /// </value>
        public Zoom Zoom { get; set; }
        /// <summary>
        /// Gets or sets the texture.
        /// </summary>
        /// <value>
        /// The image of this item
        /// </value>
        public Texture2D Texture { get; set; }
        /// <summary>
        /// Gets or sets the model.
        /// </summary>
        /// <value>
        /// Link to all the data used by the program. Will be used to have contact with other elements of the game.
        /// </value>
        public GameModel Model { get; set; }
        public bool Over { get; set; }

        /// <summary>
        /// On screen position, according to the current zoom
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public Vector2 Position
        {
            get { return position; }
            set { this.moveTo(value.X, value.Y); }
        }

        /// <summary>
        /// Gets the size (width and height) of the item
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public Dimension Size
        {
            get
            {
                return new Dimension(this.Texture.Width * this.Zoom.GameZoom, this.Texture.Height * this.Zoom.GameZoom);
            }
        }

        /// <summary>
        /// Gets or sets the center of the item. The "position" variable is actually used when getting or setting this variable.
        /// </summary>
        /// <value>
        /// The center.
        /// </value>
        public Vector2 Center
        {
            get
            {
                Vector2 position = this.Position;
                Dimension size = this.Size;
                return new Vector2(position.X + size.Width / 2f, position.Y + size.Height / 2f);
            }

            set
            {
                position.X = value.X - this.Size.Width / 2f;
                position.Y = value.Y - this.Size.Height / 2f;

            }
        }

        /// <summary>
        /// Gets the left position.
        /// </summary>
        /// <value>
        /// The left.
        /// </value>
        public float Left
        {
            get
            {
                return this.Position.X;
            }
        }

        /// <summary>
        /// Gets the right position.
        /// </summary>
        /// <value>
        /// The right.
        /// </value>
        public float Right
        {
            get
            {
                return this.Position.X + this.Size.Width;
            }
        }

        /// <summary>
        /// Gets the top position.
        /// </summary>
        /// <value>
        /// The top.
        /// </value>
        public float Top
        {
            get
            {
                return this.Position.Y;
            }
        }

        /// <summary>
        /// Gets the bottom poition.
        /// </summary>
        /// <value>
        /// The bottom.
        /// </value>
        public float Bottom
        {
            get
            {
                return this.Position.Y + this.Size.Height;
            }
        }

        /// <summary>
        /// Gets the x coordinate.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        public float X
        {
            get
            {
                return this.Position.X;
            }
        }

        /// <summary>
        /// Gets the y coordinate.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        public float Y
        {
            get
            {
                return this.Position.Y;
            }
        }

        public Item(GameModel model, Vector2 position, string imageName, float internalZoom, PowerUpZoom powerUpZoom)
        {
            position.X *= Zoom.GlobalZoom / 2.5f;
            position.Y *= Zoom.GlobalZoom / 2.5f;
            this.Position = position;
            this.Model = model;
            this.Zoom = new Zoom(internalZoom, powerUpZoom, Model);
            this.LoadContent(imageName);
            this.Over = false;
        }

        /// <summary>
        /// Updates the item to its next state according to the specified game time.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public abstract void Update(GameTime gameTime);

        /// <summary>
        /// Draws the specified sprite batch on the screen.
        /// </summary>
        /// <param name="spriteBatch">The sprite batch.</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(this.Texture, this.Position, new Rectangle?(), Color.White, 0f, new Vector2(0, 0), this.Zoom.GameZoom, SpriteEffects.None, 0f);
        }

        /// <summary>
        /// Loads the image used to display the item.
        /// </summary>
        /// <param name="imageName">Name of the image.</param>
        public virtual void LoadContent(string imageName)
        {
            this.Texture = Resources.LoadImage(imagePath + imageName, this.Model.Game);
        }

        /// <summary>
        /// Moves the item on the screen
        /// </summary>
        /// <param name="position">The position.</param>
        public void moveTo(Vector2 position)
        {
            this.moveTo(position.X, position.Y);
        }

        /// <summary>
        /// Moves the item on the screen.
        /// </summary>
        /// <param name="newX">The new x.</param>
        /// <param name="newY">The new y.</param>
        public void moveTo(float newX, float newY)
        {
            this.position.X = newX;
            this.position.Y = newY;
        }
    }
}
