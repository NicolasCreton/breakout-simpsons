﻿using BreakOutItem;
using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutUtil
{
    public class VaryingSpeed : Speed
    {
        private float step;

        /// <summary>
        /// Gets or sets the step.
        /// </summary>
        /// <value>
        /// The step.
        /// </value>
        public float Step
        {
            get
            {
                return step;
            }
            set
            {
                step = value;
            }
        }

        private float initSpeedValue;

        /// <summary>
        /// Gets or sets the speed used - Speed of the item
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public override float Value
        {
            get { return base.Value; }
            set
            {
                base.Value = value < this.MinValue ? this.MinValue : value;
                base.Value = base.Value > this.MaxValue ? this.MaxValue : base.Value;
                base.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the maximum increment.
        /// </summary>
        /// <value>
        /// The maximum increment.
        /// </value>
        public float MaxIncrement { get; set; }

        /// <summary>
        /// Gets the maximum value.
        /// </summary>
        /// <value>
        /// The maximum value.
        /// </value>
        public float MaxValue
        {
            get { return initSpeedValue + MaxIncrement * step; }
        }

        /// <summary>
        /// Gets or sets the maximum decrement.
        /// </summary>
        /// <value>
        /// The maximum decrement.
        /// </value>
        public float MaxDecrement { get; set; }

        /// <summary>
        /// Gets the minimum speed value.
        /// </summary>
        /// <value>
        /// The minimum value.
        /// </value>
        public float MinValue
        {
            get { return initSpeedValue + MaxDecrement * step; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VaryingSpeed"/> class.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="speedValue">The speed value.</param>
        /// <param name="movingDirection">The moving direction.</param>
        /// <param name="maxDecrement">The maximum decrement.</param>
        /// <param name="maxIncrement">The maximum increment.</param>
        /// <param name="step">The step.</param>
        public VaryingSpeed(GameModel model, float speedValue, Vector2 movingDirection, int maxDecrement, int maxIncrement, float step)
            : base(model, speedValue, movingDirection)
        {
            this.MovingDirection = movingDirection;
            this.MaxDecrement = maxDecrement;
            this.MaxIncrement = maxIncrement;
            this.step = step;
            this.initSpeedValue = speedValue;
            this.Value = speedValue;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VaryingSpeed"/> class.
        /// </summary>
        /// <param name="speed">The speed.</param>
        public VaryingSpeed(VaryingSpeed speed)
            : this(speed.Model, speed.Value, new Vector2(speed.MovingDirection.X, speed.MovingDirection.Y), (int)speed.MaxDecrement, (int)speed.MaxIncrement, speed.Step)
        {

        }

        /// <summary>
        /// Increments the speed.
        /// </summary>
        public void IncrementSpeed()
        {
            this.Value = this.Value + step;
        }

        /// <summary>
        /// Decrements the speed.
        /// </summary>
        public void DecrementSpeed()
        {
            this.Value = this.Value - step;
        }

        /// <summary>
        /// Implements the operator ++ to increment speed
        /// </summary>
        /// <param name="speed">The speed.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static VaryingSpeed operator ++(VaryingSpeed speed)
        {
            speed.IncrementSpeed();
            return speed;
        }

        /// <summary>
        /// Implements the operator -- to decrement speed
        /// </summary>
        /// <param name="speed">The speed.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static VaryingSpeed operator --(VaryingSpeed speed)
        {
            speed.DecrementSpeed();
            return speed;
        }

    }

}
