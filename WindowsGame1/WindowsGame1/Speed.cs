﻿using BreakOutItem;
using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutUtil
{
    public class Speed
    {
        private GameModel model;

        public GameModel Model
        {
            get { return model; }
            set { model = value; }
        }
        
        private float speedValue;

        /// <summary>
        /// Gets or sets the speed used - Speed of the item
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public virtual float Value
        {
            get
            {
                return speedValue * this.ResolutionSpeed;
            }
            set
            {
                this.speedValue = value;
            }
        }

        /// <summary>
        /// Gets the resolution speed used to adapt the speed to the user screen
        /// </summary>
        /// <value>
        /// The resolution speed.
        /// </value>
        public float ResolutionSpeed
        {
            get
            {
                return this.Model.Game.Graphics.PreferredBackBufferWidth / 1920.0f;
            }
        }

        /// <summary>
        /// Gets the position change - The next position after the update
        /// </summary>
        /// <value>
        /// The position change.
        /// </value>
        public Vector2 PositionChange
        {
            get
            {
                return this.MovingDirection * this.Value;
            }
        }

        private Vector2 movingDirection;

        /// <summary>
        /// Gets or sets the moving direction. Direction used to move the item.
        /// </summary>
        /// <value>
        /// The moving direction.
        /// </value>
        public Vector2 MovingDirection
        {
            get { return movingDirection; }
            set { movingDirection = value; }
        }

        public double Angle
        {
            get
            {
                return Math.Atan2(-movingDirection.Y, movingDirection.X);
            }
            set
            {
                this.movingDirection.X = (float)Math.Cos(value);
                this.movingDirection.Y = -(float)Math.Sin(value);
            }
        }

        public Speed(GameModel model, float speedValue, Vector2 movingDirection)
        {
            this.Model = model;
            this.Value = speedValue;
            this.MovingDirection = movingDirection;
        }

        /// <summary>
        /// Implements the operator + to add a speed value to the current speed
        /// </summary>
        /// <param name="speed">The speed.</param>
        /// <param name="speedValue">The speed value.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static Speed operator +(Speed speed, float speedValue)
        {
            speed.Value = speed.Value / speed.ResolutionSpeed + speedValue;
            return speed;
        }

        /// <summary>
        /// Multiplies the moving direction by 1 or -1 (mainly)
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public void MultiplyMovingDirection(int x, int y)
        {
            this.movingDirection.X *= x;
            this.movingDirection.Y *= y;
            this.movingDirection.Normalize();
        }
    }
}
