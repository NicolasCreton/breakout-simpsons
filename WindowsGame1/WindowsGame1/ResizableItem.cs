﻿using BreakOut;
using BreakOutUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutItem
{
    namespace BreakOutItemTags
    {
        /// <summary>
        /// The item can be affected by power ups to change their size (paddle and ball)
        /// </summary>
        public interface ResizableItem
        {
            void Grow();
            void Shrink();
        }
    }
}
