﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutUtil
{
    /// <summary>
    /// Coordinates in a 2D Array
    /// </summary>
    public class TabCoordinates
    {
        public int I { get; set; }
        public int J { get; set; }

        public TabCoordinates(int i, int j)
        {
            this.I = i;
            this.J = j;
        }

        public TabCoordinates()
            : this(0, 0)
        {

        }
    }
}
