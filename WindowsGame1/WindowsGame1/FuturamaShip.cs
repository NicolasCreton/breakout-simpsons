﻿using BreakOutModel;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutItem
{
    /// <summary>
    /// Used by BallSaviorPowerUp
    /// </summary>
    public class FuturamaShip : Item
    {
        private const string imageName = "PowerUp/pu_rope_item";
        private const float internalZoom = 0.05f;

        public FuturamaShip(GameModel model) : base(model,Vector2.Zero,imageName,internalZoom,null)
        {

        }

        /// <summary>
        /// Ensures the ship is always under the lowest unsticked ball.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            Ball selectedBall = null;
            foreach (Ball ball in this.Model.Balls)
            {
                if ((selectedBall == null || (selectedBall.Position.Y + selectedBall.Size.Height < ball.Position.Y + selectedBall.Size.Height) && !ball.Sticked))
                {
                    selectedBall = ball;
                }
                float paddleTop = this.Model.Paddle.Position.Y;
                float ballBottom = ball.Position.Y + ball.Size.Height;
                if (ballBottom < paddleTop) {
                    this.Center = new Vector2(selectedBall.Center.X, this.Model.Paddle.Center.Y);
                }
                else
                {
                    this.Center = new Vector2(selectedBall.Center.X, ballBottom + this.Size.Height/2 - 40*this.Zoom.SecondInternalZoom);
                }
                
            }
        }
    }
}
