﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutUtil
{
    public class Dimension
    {
        public float Width { get; set; }
        public float Height { get; set; }

        public Dimension(float width, float height)
        {
            this.Width = width;
            this.Height = height;
        }

        public Dimension()
            : this(0f, 0f)
        {

        }
    }
}
