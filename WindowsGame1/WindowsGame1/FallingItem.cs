﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOut;
using BreakOutModel;
using BreakOutUtil;

namespace BreakOutItem
{
    public abstract class FallingItem : CollidableItem
    {
        private Random randomizer = new Random();
        private float alcoholicTimer = 0;
        private float alcoholicTimerLimit = 100;
        private Vector2 randomPositionChange;
        private const int randomSpeedReducer = 4;
        private bool alcoholic = false;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="FallingItem"/> is alcoholic. If so, the moving direction and the speed will be multiplicated by some random values.
        /// </summary>
        /// <value>
        ///   <c>true</c> if alcoholic; otherwise, <c>false</c>.
        /// </value>
        public bool Alcoholic
        {
            get
            {
                return alcoholic;
            }
            set
            {
                alcoholic = value;
                if (alcoholic)
                {
                    this.alcoholicTimer = 0;
                }
            }
        }

        private Speed speed;

        public Speed Speed
        {
            get { return speed; }
            set { speed = value; }
        }

        public FallingItem(GameModel model, Vector2 position, string imageName, Speed speed, float internalZoom, PowerUpZoom powerUpZoom)
            : base(model, position, imageName, internalZoom, powerUpZoom)
        {
            // adaptation of the speed depending on the screen height, to get a proportional speed (base : speed==0.35f at 720p (cf. ball item))
            this.Speed = speed;
        }

        /// <summary>
        /// The falling item goes to his next position according to its moving direction and speed and the elapsed time since last update.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public void Step(GameTime gameTime)
        {
            float milliseconds = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            this.Position += this.Speed.PositionChange * milliseconds;
            if (this.Alcoholic)
            {
                this.AddAlcoholicPositionChange(milliseconds);
                if (this.Position.X < 0 && this.Speed.MovingDirection.X < 0) //Ensures the alcoholic added value doesn't make the ball go off limits
                {
                    this.moveTo(0, this.Position.Y);
                    this.Speed.MultiplyMovingDirection(-1, 1);
                }
                if (this.Position.X + this.Size.Width > this.Model.Game.ScreenBounds.Width && this.Speed.MovingDirection.X > 0)
                {
                    this.moveTo(this.Model.Game.ScreenBounds.Width - this.Size.Width, this.Position.Y);
                    this.Speed.MultiplyMovingDirection(-1, 1);
                }
                if (this.Position.Y < 0 && this.Speed.MovingDirection.Y < 0)
                {
                    this.moveTo(Position.X, 0);
                    this.Speed.MultiplyMovingDirection(1, -1);
                }
            }
        }

        /// <summary>
        /// Adds an alcoholic position change to the ball.
        /// </summary>
        /// <param name="elapsedMilliseconds">The elapsed milliseconds.</param>
        private void AddAlcoholicPositionChange(float elapsedMilliseconds)
        {
            if (alcoholicTimer == 0)
            {
                alcoholicTimerLimit = randomizer.Next(200) + 100;       //Every 100-300 ms (random), the alcoholic speed value and moving direction changes.
                randomPositionChange = getRandomPositionChange();
            }
            this.Position += randomPositionChange * elapsedMilliseconds;
            alcoholicTimer += elapsedMilliseconds;
            this.CheckAlcoholicTimerReset();
        }

        /// <summary>
        /// Checks if the alcoholic timer needs to be reset.
        /// </summary>
        private void CheckAlcoholicTimerReset()
        {
            if (alcoholicTimer >= alcoholicTimerLimit) //Resets the timer
            {
                alcoholicTimer = 0;
            }
        }

        /// <summary>
        /// Gets a new random position change.
        /// </summary>
        /// <returns></returns>
        private Vector2 getRandomPositionChange()
        {
            int size = 1000;
            float x = randomizer.Next(size) - (size / 2);
            float y = randomizer.Next(size) - (size / 2);
            float speed = randomizer.Next(randomSpeedReducer);
            speed = speed == 0 ? speed : 1 / speed;
            Vector2 move = new Vector2(x, y);
            move.Normalize();
            return move * this.Speed.Value * speed;
        }

        /// <summary>
        /// Checks if the item is off limits (bottom of the screen)
        /// </summary>
        /// <returns></returns>
        public virtual bool OffBottom()
        {
            if (this.Position.Y > this.Model.Game.ScreenBounds.Height)
                return true;
            return false;
        }

        /// <summary>
        /// Changes the ball's direction depending on the collision coordinate X on the racket (paddle), to get a variable reflection angle
        /// Slightly increases the ball's speed
        /// </summary>
        /// <param name="paddleLocation">The paddle location.</param>
        public void CheckPaddleCollision()
        {
            if (this.Model.Paddle.Intersects(this))
            {
                this.HitPaddle();
            }
        }
        public abstract void HitPaddle();
    }
}
