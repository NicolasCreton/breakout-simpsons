﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BreakOutModel;
using Microsoft.Xna.Framework;
using System.Diagnostics;
using BreakOutUtil;

namespace BreakOutItem
{
    namespace BreakOutPowerUp
    {
        public class AlcoholicPowerUp : PowerUp
        {
            private const string powerUpImageName = "PowerUp\\LowRes\\pu_alcooholic_LowRes";
            private const float internalZoom = 0.25f;

            public AlcoholicPowerUp(GameModel model, Vector2 startPosition)
                : base(model, startPosition, powerUpImageName, internalZoom)
            {
                this.Timer = 15000;
            }

            /// <summary>
            /// Power Up Starting effect
            /// </summary>
            public override void LaunchPower()
            {

                foreach (Ball ball in this.Model.Balls)
                {
                    ball.Alcoholic = true;
                }

            }

            public override void StopPower()
            {
                foreach (Ball ball in this.Model.Balls)
                {
                    ball.Alcoholic = false;
                }
            }
        }
    }
}

