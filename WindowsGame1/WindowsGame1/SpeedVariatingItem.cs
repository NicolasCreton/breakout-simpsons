﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutItem
{
    namespace BreakOutItemTags
    {
        /// <summary>
        /// The item can have its speed changed by a power up (ball)
        /// </summary>
        public interface SpeedVariatingItem
        {
            void Accelerate();
            void SlowDown();
        }
    }
}
