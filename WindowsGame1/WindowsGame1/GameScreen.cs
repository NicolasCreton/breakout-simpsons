﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOut;
using BreakOutItem;
using BreakOutItem.BreakOutPowerUp;
using BreakOutModel;
using BreakOutItem.BreakOutBrick;
using BreakOutScreen.ScreenButton;
using BreakOutResources;

namespace BreakOutScreen
{
    /// <summary>
    /// Subclass of the SpriteScreen
    /// this is the main game screen wher you play the BreakOut
    /// </summary>
    public class GameScreen : SpriteScreen
    {
        public SoundEffect BrickSound { get; set; }
        public List<SoundEffect> PaddleSounds { get; set; }
        /*public SoundEffect PaddleSound { get; set; }
        public SoundEffect PaddleSound2 { get; set; }
        public SoundEffect PaddleSound3 { get; set; }
        public SoundEffect PaddleSound4 { get; set; }
        public SoundEffect PaddleSound5 { get; set; }*/
        public SoundEffect LooseSound { get; set; }
        public SoundEffect WinSound { get; set; }
        public ClickableButton LivesRemaining { get; set; }

        private MouseState oldMouseState;
        private MouseState mouseState;

        public bool Clicked
        {
            get
            {
                return mouseState.LeftButton == ButtonState.Pressed && oldMouseState.LeftButton == ButtonState.Released;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameScreen"/> class.
        /// </summary>
        /// <param name="model">The model.</param>
        public GameScreen(GameModel model)
            : base(model, Rectangle.Empty)
        {
            this.LivesRemaining = new ClickableButton("Lives : ", false, Color.Black);
            this.ButtonList.Add(this.LivesRemaining);
            this.PaddleSounds = new List<SoundEffect>();
        }

        /// <summary>
        /// Loads the content.
        /// </summary>
        /// <param name="content">The content</param>
        /// Load the content of the screen like background and font of the different buttons
        public override void LoadContent(ContentManager content)
        {
            BrickSound = Resources.LoadSoundEffect("Sounds\\Brick",content);
            PaddleSounds.Add(Resources.LoadSoundEffect("Sounds\\Doh", content));
            PaddleSounds.Add(Resources.LoadSoundEffect("Sounds\\Doh2", content));
            PaddleSounds.Add(Resources.LoadSoundEffect("Sounds\\Doh3", content));
            PaddleSounds.Add(Resources.LoadSoundEffect("Sounds\\Doh4", content)); 
            PaddleSounds.Add(Resources.LoadSoundEffect("Sounds\\Doh5", content));
            LooseSound = Resources.LoadSoundEffect("Sounds\\loose", content);
            WinSound = Resources.LoadSoundEffect("Sounds\\win", content); 
            this.Texture = Resources.LoadImage("Pictures\\background", content);
            base.LoadContent(content);
            this.LivesRemaining.Font = this.SmallFont;
            placeString(this.ButtonList, 100, 12, 1.1f);
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Handles the input in the screen for the mouse
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyBordSate">The old key bord sate.</param>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        public override void HandleInput(KeyboardState keyboardState, KeyboardState oldKeyBordSate, MouseState mouseState, MouseState oldMouseState)
        {
            if (this.Model.Game.IsActive)
            {
                this.mouseState = mouseState;
                this.oldMouseState = oldMouseState;
                if (Clicked)
                {
                    this.Model.Paddle.LaunchBall();
                }
                base.HandleInput(keyboardState, oldKeyBordSate, mouseState, oldMouseState);
            }
            else
            {
                this.Model.Game.SelectedScreen = this.Model.Game.PauseScreen;
            }
        }


        /// <summary>
        /// Draws the game
        /// Does not override the Draw function because we don't want to see the cursor in this screen
        /// </summary>
        /// <param name="spriteBatch">The sprite batch.</param>
        /// <param name="gameTime">The game time.</param>
        public void DrawGame(SpriteBatch spriteBatch, Microsoft.Xna.Framework.GameTime gameTime)
        {
            spriteBatch.Draw(this.Texture, Vector2.Zero, new Rectangle?(), Color.White, 0f, new Vector2(0, 0), 1.1f, SpriteEffects.None, 0f);
            drawButtons(this.ButtonList, spriteBatch, 1f);
            this.Model.Paddle.Draw(spriteBatch);
            foreach (Ball ball in this.Model.Balls)
            {
                ball.Draw(spriteBatch);
            }

            foreach (Brick brick in this.Model.Level.Bricks)
            {
                if (brick != null)
                {
                    brick.Draw(spriteBatch);
                }
            }

            foreach (PowerUp powerUp in this.Model.FallingPowerUps)
            {
                powerUp.Draw(spriteBatch);
            }
        }

        /// <summary>
        /// Updates the specified game time.
        /// This function updates the lives remaining and the differents items in this screen
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            this.LivesRemaining.TextButton = "Lives : " + this.Model.Lives;
            this.Model.UpdatePlayItems(gameTime);
            this.Model.CheckWinOrLose();
            base.Update(gameTime);
        }

        /// <summary>
        /// Updates the window title.
        /// </summary>
        public override void UpdateWindowTitle()
        {
            this.Model.Game.Window.Title = string.Format("{0} - {1}", this.Model.Level.Name, GameModel.GameTitle);
        }
    }
}
