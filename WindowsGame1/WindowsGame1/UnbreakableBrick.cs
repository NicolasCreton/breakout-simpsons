﻿using BreakOut;
using BreakOutItem.BreakOutBrick;
using BreakOutUtil;
using BreakOutModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakOutItem
{
    namespace BreakOutBrick
    {
        class UnbreakableBrick : Brick
        {
            private const float internalZoom = 0.28f;

            public static char Symbol
            {
                get { return '+'; }
            }

            public const string BrickImageName = "bricks\\LowRes\\b_unbreakable_thick_LowRes";
            public const string SoundName = "unbreakableBrick";

            public UnbreakableBrick(GameModel model, Vector2 position, TabCoordinates coordinates)
                : base(model, position, coordinates, BrickImageName, SoundName, internalZoom)
            {

                this.Solid = true;
            }

            /// <summary>
            /// This brick can't break, so this function only plays a sound.
            /// </summary>
            public override void GetHit()
            {
                if (this.Sound != null)
                {
                    this.Sound.Play();
                }
            }
        }
    }
}