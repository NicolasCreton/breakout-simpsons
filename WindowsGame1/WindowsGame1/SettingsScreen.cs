﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BreakOutScreen;
using Microsoft.Xna.Framework;
using BreakOut;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using BreakOutModel;
using BreakOutScreen.ScreenButton;
using BreakOutResources;

namespace BreakOutScreen
{
    /// <summary>
    /// Subclass of the SpriteScreen
    /// This is the settings screen wher you can see and set the different options
    /// </summary>
    public class SettingsScreen : SpriteScreen
    {
        public ClickableButton SwitchToFullscreenButton { get; set; }
        public ClickableButton TitleSetting { get; set; }
        public ClickableButton MuteButton { get; set; }
        public ClickableButton AboutButton { get; set; }
        public ClickableButton ReturnButton { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="SettingsScreen"/> class.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <param name="model">The model.</param>
        public SettingsScreen(Rectangle location, GameModel model)
            : base(model, location)
        {
            this.TitleSetting = new ClickableButton("Settings", false, Color.Brown);
            this.SwitchToFullscreenButton = new ClickableButton("Switch to normal screen (Tab)", true, Color.Black);
            this.MuteButton = new ClickableButton("Mute game (M)", true, Color.Black);
            this.AboutButton = new ClickableButton("About this game (A)", true, Color.Black);
            this.ReturnButton = new ClickableButton("Return", true, Color.Black);
            this.ButtonList.Add(this.TitleSetting);
            this.ButtonList.Add(this.SwitchToFullscreenButton);
            this.ButtonList.Add(this.MuteButton);
            this.ButtonList.Add(this.AboutButton);
            this.ButtonList.Add(this.ReturnButton);
        }

        /// <summary>
        /// Loads the content.
        /// </summary>
        /// <param name="content">The content</param>
        /// Load the content of the screen like background and font of the different buttons
        public override void LoadContent(ContentManager content)
        {
            base.LoadContent(content);
            this.Texture = Resources.LoadImage("Pictures\\backSettings", content);
            this.TitleSetting.Font = this.TitleFont;
            this.SwitchToFullscreenButton.Font = this.NormalFont;
            this.MuteButton.Font = this.NormalFont;
            this.AboutButton.Font = this.NormalFont;
            this.ReturnButton.Font = this.NormalFont;
            placeString(this.ButtonList, 100, 3, 7);
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Handles the input and the buttons in the screen
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyBordSate">The old key bord sate.</param>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        public override void HandleInput(KeyboardState keyboardState, KeyboardState oldKeyBordSate, MouseState mouseState, MouseState oldMouseState)
        {
            var mousePosition = new Point(mouseState.X, mouseState.Y);
            checkIfMouseOver(this.ButtonList, mousePosition, mouseState);

            switchMuteFromSettings(mouseState, oldMouseState, mousePosition);
            switchScreenSizeFromSettings(mouseState, oldMouseState, mousePosition);
            getBack(keyboardState, oldKeyBordSate, mouseState, oldMouseState, mousePosition);
            aboutThisGame(keyboardState, oldKeyBordSate, mouseState, oldMouseState, mousePosition);


            base.HandleInput(keyboardState, oldKeyBordSate, mouseState, oldMouseState);
        }

        /// <summary>
        /// Switches the mute from settings.
        /// </summary>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        /// <param name="mousePosition">The mouse position.</param>
        public void switchMuteFromSettings(MouseState mouseState, MouseState oldMouseState, Point mousePosition)
        {
            if (this.MuteButton.chekIfCliked(mousePosition, mouseState, oldMouseState, this.NormalFont))
            {
                toogleMute();
            }
        }

        /// <summary>
        /// Switches the mute from anywhere.
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyBordSate">The old key bord sate.</param>
        public void switchMuteFromAnywhere(KeyboardState keyboardState, KeyboardState oldKeyBordSate)
        {
            if (keyboardState.IsKeyDown(Keys.M) && oldKeyBordSate.IsKeyUp(Keys.M))
            {
                toogleMute();
            }
        }

        /// <summary>
        /// Toogles the mute.
        /// </summary>
        public void toogleMute()
        {
            if (MediaPlayer.IsMuted == true)
            {
                MediaPlayer.IsMuted = false;
                SoundEffect.MasterVolume = 1;
                this.MuteButton.TextButton = "Mute game (M)";
            }
            else
            {
                MediaPlayer.IsMuted = true;
                SoundEffect.MasterVolume = 0;
                this.MuteButton.TextButton = "Unmute game (M)";
            }
        }

        /// <summary>
        /// Switches the screen size from settings.
        /// </summary>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        /// <param name="mousePosition">The mouse position.</param>
        public void switchScreenSizeFromSettings(MouseState mouseState, MouseState oldMouseState, Point mousePosition)
        {
            if (this.SwitchToFullscreenButton.chekIfCliked(mousePosition, mouseState, oldMouseState, this.NormalFont))
            {
                toogleFullScreen();
            }
        }

        /// <summary>
        /// Switches the screen size from anywhere.
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyBordSate">The old key bord sate.</param>
        public void switchScreenSizeFromAnywhere(KeyboardState keyboardState, KeyboardState oldKeyBordSate)
        {
            if (keyboardState.IsKeyDown(Keys.Tab) && oldKeyBordSate.IsKeyUp(Keys.Tab))
            {
                toogleFullScreen();
            }
        }

        /// <summary>
        /// Toogles the full screen.
        /// </summary>
        public void toogleFullScreen()
        {
            if (this.Model.Game.Graphics.IsFullScreen == true)
            {
                this.Model.Game.Graphics.ToggleFullScreen();
                this.SwitchToFullscreenButton.TextButton = "Switch to full screen (Tab)";
            }
            else
            {
                this.Model.Game.Graphics.ToggleFullScreen();
                this.SwitchToFullscreenButton.TextButton = "Switch to normal screen (Tab)";
            }
        }

        /// <summary>
        /// get back to the menu Screen
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyBordSate">The old key bord sate.</param>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        /// <param name="mousePosition">The mouse position.</param>
        public void getBack(KeyboardState keyboardState, KeyboardState oldKeyBordSate, MouseState mouseState, MouseState oldMouseState, Point mousePosition)
        {
            if (keyboardState.IsKeyDown(Keys.Escape) && oldKeyBordSate.IsKeyUp(Keys.Escape) || this.ReturnButton.chekIfCliked(mousePosition, mouseState, oldMouseState, this.NormalFont))
            {
                this.Model.Game.SelectedScreen = this.Model.Game.MenuScreen;
            }
        }

        /// <summary>
        /// Abouts the this game button input.
        /// </summary>
        /// <param name="keyboardState">State of the keyboard.</param>
        /// <param name="oldKeyBordSate">The old key bord sate.</param>
        /// <param name="mouseState">State of the mouse.</param>
        /// <param name="oldMouseState">Old state of the mouse.</param>
        /// <param name="mousePosition">The mouse position.</param>
        public void aboutThisGame(KeyboardState keyboardState, KeyboardState oldKeyBordSate, MouseState mouseState, MouseState oldMouseState, Point mousePosition)
        {
            if (keyboardState.IsKeyDown(Keys.A) && oldKeyBordSate.IsKeyUp(Keys.A) || this.AboutButton.chekIfCliked(mousePosition, mouseState, oldMouseState, this.NormalFont))
            {
                this.Model.Game.SelectedScreen = this.Model.Game.AboutScreen;
            }
        }

        /// <summary>
        /// Draws the cursor and the buttons in the screen
        /// The cursor chagnes when the mouse is over a ClickableButton
        /// </summary>
        /// <param name="spriteBatch">The sprite batch.</param>
        /// <param name="gameTime">The game time.</param>
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {

            spriteBatch.Draw(this.Texture, this.Location, this.Tint);
            drawButtons(this.ButtonList, spriteBatch, 1f);

            base.Draw(spriteBatch, gameTime);
        }

        /// <summary>
        /// Updates the specified game time.
        /// This function updates the mouse position in the screen
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}
